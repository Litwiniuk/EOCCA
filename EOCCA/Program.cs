﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using Accord.MachineLearning;
using ExperimentDatabase;
using EOCCA.Benchmarks;
using EOCCA.Benchmarks.Models;
using EOCCA.Shared;
using Accord.Statistics.Analysis;
using System.IO;
using System.Globalization;

namespace EOCCA
{
    class Program
    {
        static void Main(string[] args)
        {
            SetDotCulture();
            try
            {
                if (args != null & args.Any())
                {
                    StartByArgs(args);
                    return;
                }

                #region params
                int k_min = 2;
                int n = 2;
                int points = 100000;
                int trainingPointsNumber = 500;
                int[] B = new int[] { 1, 1 };

                int seed = 10;
                ClusteringMethods clusteringMethod = ClusteringMethods.PyXMEANS;
                double extraRadius = 1.1;
                int clusters = 2;

                bool std = false;
                bool pca = false;

                bool startVisualization = true;
                bool saveResults = true;

                Debug.Assert(k_min == B.Length);

                #endregion

                //StartSingleExperiment(k_min, n, B, seed, trainingPointsNumber, points, std, pca, saveResults, startVisualization, extraRadius, clusters, clusteringMethod, new StandarizedBallExperiment());
                //StartSingleExperiment(k_min, n, B, seed, trainingPointsNumber, points, std, pca, saveResults, startVisualization, extraRadius, clusters, clusteringMethod, new CubeExperiment());
                //StartSingleExperiment(k_min, n, B, seed, trainingPointsNumber, points, std, pca, saveResults, startVisualization, extraRadius, clusters, clusteringMethod, new SimplexExperiment());

                DataSetsGenerator.Generate(trainingPoints: 500, testingPoints: 1000000,
                                    kMin: 1, kMax: 3,
                                    nMin: 3, nMax: 8,
                                    seedMin: 0, seedMax: 20);

                //Stopwatch sw = new Stopwatch();

                //sw.Start();
                Start("Data/", 0, 0);
                //sw.Stop();

                new CaseStudy().Start();

                //Console.WriteLine("Elapsed={0}", sw.Elapsed);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static void StartByArgs(string[] args)
        {
            if (args[0] == "by_file")
            {
                string path = args[1];
                int seedMin = Convert.ToInt32(args[2]);
                int seedMax = Convert.ToInt32(args[3]);
                string filePath = args[4];
                StartTuningOnFile(path, seedMin, seedMax, filePath);
            }
            else if (args[1] == "by_params")
            {

            }
        }

        public static void SetDotCulture()
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";

            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        public static void Start(string path, int seedMin, int seedMax)
        {
            Stopwatch sw = new Stopwatch();

            sw.Start();
            string[] filesPath = Directory.GetFiles($"{path}/Train/");

            foreach (string filePath in filesPath)
            {
                StartTuningOnFile(path, seedMin, seedMax, filePath);
            }
            sw.Stop();

            Console.WriteLine("Elapsed={0}", sw.Elapsed);
        }

        private static void StartTuningOnFile(string path, int seedMin, int seedMax, string filePath)
        {
            string name = Path.GetFileName(filePath);
            Console.WriteLine($"Start tuning on {name} with params: seedMin = {seedMin}, seedMax={seedMax}, filePath={filePath}");
            DataModel trainSet = FileHelper.ReadDataSetFromCSV($"{path}/Train/{name}");
            DataModel testSet = FileHelper.ReadDataSetFromCSV($"{path}/Test/{name}");
            DataModel validSet = FileHelper.ReadDataSetFromCSV($"{path}/Valid/{name}");

            double[][] train = trainSet.Dataset;
            double[][] test = testSet.Dataset;
            double[][] valid = validSet.Dataset;

            Console.WriteLine($"Train size: {train.Length}, Test size: {test.Length}, Valid size: {valid.Length}");

            bool[] testLabels = testSet.Labels;
            int n = trainSet.N;
            int k = trainSet.K;
            string fileName = trainSet.FileName;

            TuningParameters(n, seedMin, seedMax, train, test, testLabels, valid, fileName);
        }

        public static void StartSingleExperiment(int k_min, int n, int[] B, int seed, int trainingPointsNumber, int points, bool std, bool pca, bool saveResults, bool startVisualization, double extraRadius, int clusters, ClusteringMethods clusteringMethod, IExperiment experiment)
        {
            BenchmarkGenerator bg = new BenchmarkGenerator(experiment, k_min, n, B, seed);

            double[][] trainSet = bg.GeneratePointsSet(trainingPointsNumber, true);
            double[][] testSet = bg.GeneratePointsSet(points);
            double[][] validationSet = bg.GeneratePointsSet(points);
            bool[] testLabels = bg.GetLabels(testSet);

            Stopwatch sw = new Stopwatch();

            sw.Start();
            SingleExperiment(seed, clusteringMethod, extraRadius, clusters,
                                trainSet, testSet, testLabels,
                                validationSet,
                                std, pca, saveResults, startVisualization, saveToDB: false);
            sw.Stop();

            Console.WriteLine("Elapsed={0}", sw.Elapsed);
        }

        public static void SingleExperiment(int seed, ClusteringMethods clusteringMethod, double extraRadius, int clusters, double[][] trainSet, double[][] testSet, bool[] testLabels, double[][] validationSet, bool std, bool pca, bool saveResults, bool startVisualization, string fileName = "", string info = "", bool saveToDB = true, string clustersInStr = "")
        {
            try
            {
                if (saveToDB && DBHelper.CheckIsExperimentExists(DBHelper.ConstructArgsString(seed, std, pca, clusters, extraRadius, clusteringMethod, trainSet.Length, fileName, info)))
                {
                    return;
                }

                Accord.Math.Random.Generator.Seed = seed;
                Process currentProcess = Process.GetCurrentProcess();
                currentProcess.Refresh();
                var prevTime = currentProcess.TotalProcessorTime.TotalSeconds + PythonExe.TotalProcessorTime;
                var model = new EOCCA(clusteringMethod, clusters, extraRadius, saveResults, std, pca);
                model.Fit(trainSet);
                currentProcess.Refresh();
                string totalTime = (currentProcess.TotalProcessorTime.TotalSeconds + PythonExe.TotalProcessorTime - prevTime).ToString(CultureInfo.InvariantCulture);

                bool[] yPredictTrain = model.Predict(trainSet);

                bool[] yPredictValid = model.Predict(validationSet);

                int positivesValid = yPredictValid.Count(x => x == true);
                var xp = testLabels.Where(a => a).Count();
                bool[] yPredict = model.Predict(testSet);
                int positivesTest = yPredict.Count(x => x == true);

                ConfusionMatrix trainConfusionMatrix = MakeConfusionMatrix(Enumerable.Repeat(true, trainSet.Length).ToArray(), yPredictTrain);
                ConfusionMatrix testConfusionMatrix = MakeConfusionMatrix(testLabels, yPredict);

                int outClusters = model.GetClustersCount();
                double objectiveFunction = ObjectiveFunctions.CountDefaultObjectiveFunction(testConfusionMatrix.Recall, positivesValid, yPredictValid.Count());

                double matthew = ObjectiveFunctions.CountMatthewsCorrelationCoefficient(testConfusionMatrix);
                string constraints = string.Join("\n", model.GetConstraints());
                if (saveResults)
                {
                    SaveAndStartVisualization(testSet, startVisualization, yPredict);
                }

                if (saveToDB)
                {
                    DBHelper.SaveExperiment(seed, std, pca, objectiveFunction, clusteringMethod, clusters, outClusters, extraRadius, testConfusionMatrix, trainConfusionMatrix, trainSet.Length, totalTime, fileName, info, constraints, clustersInStr, yPredictValid.Count(), positivesValid, yPredict.Count(), positivesTest);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static void SaveAndStartVisualization(double[][] testSet, bool startVisualization, bool[] y_predict)
        {
            DataModel pm = new DataModel()
            {
                Dataset = testSet,
                Labels = y_predict
            };

            FileHelper.SaveJsonToFile("Predict", pm);

            if (startVisualization)
            {
                PythonExe.runCmd("./Benchmarks/visualization.py");
            }
        }

        public static void TuningParameters(int n, int seedMin, int seedMax, double[][] trainSet, double[][] testSet, bool[] testLabels, double[][] validationSet, string fileName)
        {
            for (int seed = seedMin; seed <= seedMax; seed++)
            {
                #region std

                bool std = false;
                SingleExperiment(seed, ClusteringMethods.PyXMEANS, 1.0, n, trainSet, testSet, testLabels, validationSet, std, true, false, false, fileName, "tuning_std", true, "n");

                std = true;
                SingleExperiment(seed, ClusteringMethods.PyXMEANS, 1.0, n, trainSet, testSet, testLabels, validationSet, std, true, false, false, fileName, "tuning_std", true, "n");

                #endregion


                #region pca

                bool pca = true;
                SingleExperiment(seed, ClusteringMethods.PyXMEANS, 1.0, n, trainSet, testSet, testLabels, validationSet, std, pca, false, false, fileName, "tuning_pca", true, "n");

                pca = false;
                SingleExperiment(seed, ClusteringMethods.PyXMEANS, 1.0, n, trainSet, testSet, testLabels, validationSet, std, pca, false, false, fileName, "tuning_pca", true, "n");

                #endregion


                #region clusters

                string[] clustersStr = new string[] { "n", "2n", "n^2", "1" };
                int[] clusters = new int[] { n, 2 * n, n * n, 1 };

                int iterator = 0;
                ClusteringMethods[] clusteringMethods = new ClusteringMethods[] { ClusteringMethods.PyXMEANS, ClusteringMethods.KMEANS };
                foreach (int clustersNumber in clusters)
                {
                    foreach (ClusteringMethods clusteringMethod in clusteringMethods)
                    {
                        SingleExperiment(seed, clusteringMethod, 1.0, clustersNumber, trainSet, testSet, testLabels, validationSet, true, true, false, false, fileName, "tuning_clusters", true, clustersStr[iterator]);
                    }
                    iterator++;
                }

                #endregion


                #region extraRadius

                double radiusMin = 0.90, radiusMax = 1.10;

                for (double radius = radiusMin; radius <= radiusMax; radius += 0.02)
                {
                    SingleExperiment(seed, ClusteringMethods.PyXMEANS, radius, 1, trainSet, testSet, testLabels, validationSet, true, true, false, false, fileName, "tuning_radius", true, "1");
                }

                #endregion

                var rand = new Random(0);
                for (int i = 5; i > 0; i--)
                {
                    string trainingSize = (100 * i).ToString();
                    SingleExperiment(seed, ClusteringMethods.PyXMEANS, 1.0, 1, trainSet.OrderBy(x => rand.Next()).Take(100 * i).ToArray(), testSet, testLabels, validationSet, true, true, false, false, fileName, "tuning_trainset_" + trainingSize, true, "1");
                }
            }
        }

        public static ConfusionMatrix MakeConfusionMatrix(bool[] expected, bool[] predicted)
        {
            Console.WriteLine($"Expected size: {expected.Length}, predicted: {predicted.Length}");
            return new ConfusionMatrix(predicted, expected);
        }
    }
}