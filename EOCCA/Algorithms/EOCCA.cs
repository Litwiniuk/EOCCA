﻿using Accord.MachineLearning;
using Accord.Statistics.Analysis;
using EOCCA.Algorithms;
using EOCCA.Benchmarks.Models;
using EOCCA.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace EOCCA
{
    public class EOCCA
    {
        private ClusteringMethods _clusteringMethod;
        private List<Cluster> _clustersObj;
        private int _clustersNumber;
        private bool _saveClusters;
        private bool _standarization;
        private bool _pca;
        private double _extraRadius;

        public EOCCA(ClusteringMethods clusteringMethod, int clustersNumber, double extraRadius = 0.0, bool saveClusters = false, bool standarization = false, bool pca = false)
        {
            this._clusteringMethod = clusteringMethod;
            this._clustersNumber = clustersNumber;
            this._saveClusters = saveClusters;
            this._extraRadius = extraRadius;
            this._standarization = standarization;
            this._pca = pca;
        }

        public void Fit(double[][] points)
        {
            //      SaveTrainingPoints(points);
            //    PythonExe.runCmd("./Benchmarks/visualization.py");

            ClusterPoints(points);
            if (_saveClusters)
            {
                SaveTrainingPoints();
                SaveClusters();
                SaveIbex();
            }
        }

        private void ClusterPoints(double[][] points)
        {
            switch (this._clusteringMethod)
            {
                case ClusteringMethods.XMEANS:
                    XMeans xmeans = new XMeans(this._clustersNumber, int.MaxValue, this._extraRadius, this._standarization, this._pca);
                    xmeans.Fit(points);
                    this._clustersObj = xmeans.ClusterList;
                    break;
                case ClusteringMethods.PyXMEANS:
                    PyXMeans pyxmeans = new PyXMeans(points, this._clustersNumber, int.MaxValue);
                    int clustersOut = pyxmeans.Fit();
                    this._clustersObj = KMeansClustering.KMeans(points, clustersOut, this._extraRadius, this._standarization, this._pca);
                    break;
                case ClusteringMethods.KMEANS:
                default:
                    this._clustersObj = KMeansClustering.KMeans(points, this._clustersNumber, this._extraRadius, this._standarization, this._pca);
                    break;
            }

            foreach (var cluster in this._clustersObj)
            {
                cluster.Transform();
            }
        }

        private void SaveTrainingPoints()
        {
            BenchmarkModel bm = new BenchmarkModel
            {
                TrainingSet = _clustersObj?.SelectMany(x => x._points).Select(x => x.ToList()).ToList(),
                TestSet = null
            };
            FileHelper.SaveJsonToFile("Training", bm);
        }

        private void SaveClusters()
        {
            List<EllipseModel> ellipses = new List<EllipseModel>();
            foreach (Cluster cluster in _clustersObj)
            {
                ellipses.Add(new EllipseModel(cluster._centroids, cluster._radiuses));
            }
            FileHelper.SaveJsonToFile("ellipses", ellipses);
        }

        private void SaveIbex()
        {
            var constraints = GetConstraints();
            Ibex.ExportIbex(constraints, "constraint.minibex");
        }

        /// <summary>
        /// Returns an AMPL model
        /// </summary>
        /// <returns></returns>
        public string ToAMPL()
        {
            var constraints = this.GetConstraints();

            var builder = new StringBuilder();
            for (int i = 0; i < this._clustersObj[0].Dimensions; ++i)
            {
                builder.AppendFormat("var x{0};\n", i);
            }
            builder.AppendFormat("var b{{{{1..{0}}}}} binary;\n", constraints.Length);
            builder.AppendLine("param M = 1e12;\n");
            
            for (int i = 0; i < constraints.Length; ++i)
            {
                builder.AppendFormat("subject to c{0}:\n{1} + M * (1 - b[{0}]);\n", i + 1, constraints[i]);
            }

            builder.AppendFormat("subject to aux:\nsum {{i in {{1..{0}}}}} b[i] >= 1;\n", constraints.Length);

            return builder.ToString();
        }

        public bool[] Predict(double[][] points)
        {
            bool[] y_predictions = new bool[points.GetLength(0)];
            int iterator = 0;
            foreach (double[] singleObservation in points)
            {
                y_predictions[iterator] = IsInClusters(singleObservation);
                iterator++;
            }

            return y_predictions;
        }

        private bool IsInClusters(double[] singleObservation)
        {
            foreach (Cluster cluster in _clustersObj)
            {
                if (cluster.IsPointInCluster(singleObservation))
                {
                    return true;
                }
            }
            return false;
        }

        public int GetClustersCount()
        {
            return _clustersObj.Count;
        }

        public string[] GetConstraints()
        {
            string[] result = new string[_clustersObj.Count];
            for (int i = 0; i < _clustersObj.Count; i++)
            {
                result[i] = _clustersObj[i].ConstructConstraint();
            }
            return result;
        }
    }
}