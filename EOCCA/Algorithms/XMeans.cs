﻿using Accord.MachineLearning;
using EOCCA.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Math;
using Accord.Statistics.Distributions.Univariate;

namespace EOCCA
{
    public class XMeans
    {
        private bool _standarization;
        private bool _pca;

        private double _extraRadius;

        private int k_min;
        private int? k_max;

        private double _threshold = 0.01;

        public List<Cluster> ClusterList { get; private set; } = new List<Cluster>();

        public XMeans(int k_min, int k_max, double extraRadius, bool standarization, bool pca)
        {
            this.k_min = k_min;
            this.k_max = k_max;
            this._extraRadius = extraRadius;
            this._pca = pca;
            this._standarization = standarization;
        }

        public void Fit(double[][] points)
        {
            List<Cluster> kmeansClusters = new List<Cluster>();
            int k = k_min;
            while (!k_max.HasValue || k <= k_max)
            {
                kmeansClusters = KMeansClustering.KMeans(points, k, this._extraRadius, _standarization, _pca);
                foreach (var cluster in kmeansClusters)
                {
                    try
                    {
                        var splitClusters = KMeansClustering.KMeans(cluster._points, 2, this._extraRadius, 
                                                                        this._standarization, this._pca);
                        var c1 = splitClusters[0];
                        var c2 = splitClusters[1];

                        var beta = c1._centroids.Subtract(c2._centroids).Euclidean() /
                                   Math.Sqrt(c1.Covariance.PseudoDeterminant() +
                                             c2.Covariance.PseudoDeterminant());

                        var alpha = 0.5 / NormalDistribution.Standard.DistributionFunction(beta);
                        var bic = -2 *
                                  (cluster.PointsNumber * Math.Log(alpha) + c1.LogLikelihood() +
                                   c2.LogLikelihood()) + 2 * cluster.K * Math.Log(cluster.PointsNumber);

                        var threshold = Math.Abs(bic - cluster.BIC) / cluster.BIC;

                        if (bic >= cluster.BIC && threshold > _threshold)
                        {
                            k++;
                        }
                    }
                    catch (Exception e)
                    {
                       // Console.WriteLine(e);
                    }
                }
                if (k == kmeansClusters.Count())
                {
                    break;
                }
            }
            this.ClusterList = kmeansClusters;
        }
    }
}
