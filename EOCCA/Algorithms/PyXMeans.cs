﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IronPython.Hosting;
using EOCCA.Benchmarks;
using EOCCA.Shared;

namespace EOCCA.Algorithms
{
    public class PyXMeans
    {
        private double[][] _points;
        private int _k_min;
        private int _k_max;
        public PyXMeans(double[][] points, int k_min, int k_max)
        {
            _points = points;
            _k_min = k_min;
            _k_max = k_max;
        }

        public int Fit()
        {
            string path = CreateTempFile();

            string args = "\"" + path + "\" " + _k_min + " " + _k_max;
            string scriptResult = PythonExe.runCmd("./Benchmarks/xmeans.py", args);
            int number;

            File.Delete(path);

            bool result = Int32.TryParse(scriptResult, out number);
            if (result)
            {
                return number;
            }

            Console.WriteLine(scriptResult);
            
            return _k_min;
        }

        private string CreateTempFile()
        {
            string fileName = "TEMP" + Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.Ticks.ToString();
            string path = $"{DataSetsGenerator.dataPath}Temp/{fileName}.csv";
            FileHelper.SaveDatasetToCSV(_points, path, true);
            return Path.GetFullPath(path);
        }
    }
}
