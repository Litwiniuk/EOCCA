﻿using Accord.MachineLearning;
using Accord.Math;
using Accord.Statistics;
using Accord.Statistics.Distributions.Fitting;
using Accord.Statistics.Distributions.Multivariate;
using EOCCA.Shared;
using System;
using System.Diagnostics;
using System.Linq;

namespace EOCCA
{
    public class Cluster
    {
        public int K { get; private set; }
        public int Dimensions { get; private set; }
        public double[][] _points { get; private set; }
        public double[] _radiuses { get; private set; }
        public double[] _centroids { get; private set; }
        public double[] Means { get; private set; }
        public double[][] Covariance { get; private set; }
        private bool _std;
        private bool _pca;

        private PrepareData dataModificator;
        private RobustMultivariateNormalDistribution _robustMultivariateNormalDistribution;
        private MultivariateNormalDistribution _multivariateNormalDistribution;

        private double _extraRadius = 0.0;

        public double BIC => -2 * LogLikelihood() + K * Math.Log(PointsNumber);
        public int PointsNumber => _points.GetLength(0);


        public Cluster(double[] centroids, double[][] points, double extraRadius, bool standarization = false, bool pca = false, Standarization baseStd = null)
        {
            Debug.Assert(extraRadius != 0.0, "Extra radius can't be 0.");

            dataModificator = new PrepareData(points, standarization, pca, baseStd);

            _extraRadius = extraRadius;
            _std = standarization;
            _pca = pca;
            //points = dataModificator.Transform(points);
            _points = points;

            //double[][] singleObs = new double[1][];
            //singleObs[0] = centroids.Clone() as double[]; // fix problem with changing original dataset
            //centroids = dataModificator.Transform(singleObs)[0];


            _centroids = centroids;
            CountRadiuses(points);


            InitMultivariateNormalDistribution();

            Dimensions = _points[0].Length;

            K = Dimensions * (Dimensions + 3) / 2;
        }

        public void Transform()
        {
            _points = dataModificator.Transform(_points);

            double[][] singleObs = new double[1][];
            singleObs[0] = _centroids.Clone() as double[]; // fix problem with changing original dataset
            _centroids = dataModificator.Transform(singleObs)[0];

            InitMultivariateNormalDistribution();

            CountRadiuses(_points);

            Debug.Assert(_centroids.Length == singleObs[0].Length);
        }

        public double LogLikelihood()
        {
            try
            {
                return _points.Sum(p => _robustMultivariateNormalDistribution.LogProbabilityDensityFunction(p));
            }
            catch
            {
                return _points.Sum(p => _multivariateNormalDistribution.LogProbabilityDensityFunction(p));
            }
        }

        private void InitMultivariateNormalDistribution()
        {
            this.Means = _points.Mean(_points.Transpose().Select(x => x.Sum()).ToArray());
            this.Covariance = _points.Covariance(Means);
            try
            {
                _robustMultivariateNormalDistribution = new RobustMultivariateNormalDistribution(Means, Covariance);
                _multivariateNormalDistribution = new MultivariateNormalDistribution(Means, Covariance);
            }
            catch (Exception e)
            //catch (Accord.NonPositiveDefiniteMatrixException ex)
            {
                Console.WriteLine(e.Message);
                _multivariateNormalDistribution = MultivariateNormalDistribution.Estimate(_points, new NormalOptions()
                {
                    Regularization = 1e-8 // this value will be added to the diagonal until it becomes positive-definite
                });
            }
        }

        private void CountRadiuses(double[][] points)
        {
            Debug.Assert(_centroids.Length == this.Dimensions || this.Dimensions == 0);
            _radiuses = new double[_centroids.Length];
            for (int centroidIndex = 0; centroidIndex < _centroids.Length; centroidIndex++)
            {
                _radiuses[centroidIndex] = FindFarthestPoint(_centroids[centroidIndex], points.Select(x => x[centroidIndex]).ToArray());
                _radiuses[centroidIndex] = Math.Max(_radiuses[centroidIndex], 1e-3);
            }
        }

        private double FindFarthestPoint(double centroid, double[] allPointsInDimension)
        {
            return allPointsInDimension.Max(x => Math.Abs(centroid - x)) * this._extraRadius;
        }

        public string ConstructConstraint()
        {
            string[] var = new string[_centroids.Length];
            for (int dimension = 0; dimension < this.Dimensions; dimension++)
            {
                var[dimension] = $"x{dimension}";
            }

            if (dataModificator._std != null)
            {
                for (int dimension = 0; dimension < this.Dimensions; dimension++)
                {
                    if (dataModificator._std.GetMeans()[dimension] == 0.0)
                        var[dimension] = $"{var[dimension]}/{dataModificator._std.GetStandardDeviations()[dimension]}";
                    else
                        var[dimension] = $"({var[dimension]} - {dataModificator._std.GetMeans()[dimension]})/{dataModificator._std.GetStandardDeviations()[dimension]}";
                }
            }

            if (dataModificator._principalComponentAnalysis != null)
            {
                var newvar = new string[_centroids.Length];
                var eigenvectors = dataModificator._principalComponentAnalysis.ComponentMatrix;
                var rows = 1;
                var cols = var.Length;
                var dimensions = dataModificator._principalComponentAnalysis.Components.Count;
                for (int i = 0; i < rows; i++)
                    for (int j = 0; j < dimensions; j++)
                        for (int k = 0; k < cols; k++)
                        {
                            if (eigenvectors[k, j] == 0.0)
                                continue;
                            if (!string.IsNullOrEmpty(newvar[j]))
                                newvar[j] += " + ";
                            if (dataModificator._principalComponentAnalysis.Means[k] == 0.0)
                                newvar[j] += $"{var[k]} * {eigenvectors[k, j]}";
                            else
                                newvar[j] += $"({var[k]} - {dataModificator._principalComponentAnalysis.Means[k]}) * {eigenvectors[k, j]}";
                        }
                var = newvar;
            }

            for (int dimension = 0; dimension < this.Dimensions; dimension++)
            {
                if (!string.IsNullOrEmpty(var[dimension]))
                    var[dimension] = $"(({var[dimension]} - {_centroids[dimension]})^2)/{Math.Pow(_radiuses[dimension], 2)}";
            }

            return string.Join(" + ", var.Where(v => !string.IsNullOrEmpty(v))) + " <= 1";
        }

        /// <summary>
        /// Numerically evaluates constraint for the given data point. Uses the same control flow as <see cref="ConstructConstraint"/> for printing constraints.
        /// The primary purpose of this method is to verify correctness of <see cref="ConstructConstraint"/>.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public bool EvalConstraint(double[] x)
        {
            double[] var = x.Clone() as double[];

            if (dataModificator._std != null)
            {
                for (int dimension = 0; dimension < this.Dimensions; dimension++)
                {
                    var[dimension] = (var[dimension] - dataModificator._std.GetMeans()[dimension]) / dataModificator._std.GetStandardDeviations()[dimension];
                }

                Debug.Assert(dataModificator._std.Transform(new[] { x })[0].Zip(var, (a, b) => a == b).All(a => a));
            }

            if (dataModificator._principalComponentAnalysis != null)
            {
                double[] newvar = new double[_centroids.Length];
                var eigenvectors = dataModificator._principalComponentAnalysis.ComponentMatrix;
                var rows = 1;
                var cols = var.Length;
                var dimensions = dataModificator._principalComponentAnalysis.Components.Count;
                for (int i = 0; i < rows; i++)
                    for (int j = 0; j < dimensions; j++)
                        for (int k = 0; k < cols; k++)
                            newvar[j] += (var[k] - dataModificator._principalComponentAnalysis.Means[k]) * eigenvectors[k, j];

                Debug.Assert(dataModificator._principalComponentAnalysis.Transform(new[] { var })[0].Zip(newvar, (a, b) => a == b).All(a => a));
                var = newvar;
            }

            for (int dimension = 0; dimension < this.Dimensions; dimension++)
            {
                var[dimension] = Math.Pow(var[dimension] - _centroids[dimension], 2) / Math.Pow(_radiuses[dimension], 2);
            }

            return var.Sum() <= 1.0;
        }

        /// <summary>
        /// https://math.stackexchange.com/questions/76457/check-if-a-point-is-within-an-ellipse
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public bool IsPointInCluster(double[] points)
        {
            double diff;
            double[][] singleObs = new double[1][];
            singleObs[0] = points.Clone() as double[]; // fix problem with changing original dataset
            double sum = 0;
            var transformedPoints = dataModificator.Transform(singleObs)[0];

            for (int dimension = 0; dimension < _centroids.Length; dimension++)
            {
                diff = (transformedPoints[dimension] - _centroids[dimension]) / _radiuses[dimension];
                sum += diff * diff;
            }

            Debug.Assert((sum <= 1) == this.EvalConstraint(points));

            return sum <= 1;
        }
    }
}
