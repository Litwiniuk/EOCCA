﻿using EOCCA.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace EOCCA.Benchmarks
{
	public class DataSetsGenerator
	{
		public static string dataPath = "Data/";

		public static void Generate(int trainingPoints, int testingPoints, int kMin, int kMax, int nMin, int nMax, int seedMin, int seedMax)
		{
			IExperiment[] experiments = new IExperiment[] { new BallExperiment(), new CubeExperiment(), new SimplexExperiment() };

			foreach (IExperiment experiment in experiments)
			{
				for (int seed = seedMin; seed < seedMax; seed++)
				{
					Accord.Math.Random.Generator.Seed = seed;

					for (int k = kMin; k <= kMax; k++)
					{
						int[] B = GenerateVectorB(k);

						for (int n = nMin; n <= nMax; n++)
						{
							BenchmarkGenerator bg = new BenchmarkGenerator(experiment, k, n, B, seed);

							string fileName = GenerateFileName(n, seed, k, experiment);
							
							// Generate sets
							if (!File.Exists($"{dataPath}/Train/{fileName}.csv"))
							{
								double[][] trainingSet = bg.GeneratePointsSet(trainingPoints, true);
								FileHelper.SaveDatasetToCSV(trainingSet, $"{dataPath}/Train/{fileName}.csv", true);
							}

							if (!File.Exists($"{dataPath}/Test/{fileName}.csv"))
							{
								double[][] testingSet = bg.GeneratePointsSet(testingPoints);
								bool[] testLabels = bg.GetLabels(testingSet);
								FileHelper.SaveDatasetToCSV(testingSet, $"{dataPath}/Test/{fileName}.csv", true, testLabels);
							}

							if (!File.Exists($"{dataPath}/Valid/{fileName}.csv"))
							{
								double[][] validationSet = bg.GeneratePointsSet(testingPoints);
								FileHelper.SaveDatasetToCSV(validationSet, $"{dataPath}/Valid/{fileName}.csv", true);
							}
						}

					}
				}
			}
		}

		private static string GenerateFileName(int n, int seed, int k, IExperiment experiment)
		{
			return $"{experiment.GetName()}_{n}_{k}_{seed}";
		}

		private static int[] GenerateVectorB(int k)
		{
			int[] result = new int[k];
			for (int i = 0; i < k; i++)
			{
				result[i] = 1;
			}
			return result;
		}
	}
}
