import sqlite3

from experimentdatabase import Database

def main():
    source_db_filename = "statistics.sqlite"
    destination_db_filename = "ESOCCS_for_EOCCA.sqlite"

    name = "EOCCA"

    source_db_conn = sqlite3.connect(source_db_filename)
    source_cursor = source_db_conn.cursor()
    source_cursor.execute(r'''SELECT 
                                substr(file, 1, length(file)-4) AS in_problem, --0
                                n,
                                k, 
                                trainingPointsSize AS in_sample, 
                                cast(trim(substr(file, length(file)-5, 2), "_") AS INT) AS in_seed, 
                                true_positives, --5
                                false_positives,
                                true_negatives,
                                false_negatives,
                                fscore,
                                time --10
                                FROM constraints
                                WHERE info="tuning_trainset_500"''')

    destination_db = Database(destination_db_filename)

    for row in source_cursor:
        exp = destination_db.new_experiment()
        exp["in_linear"] = True
        exp["in_quadratic"] = True
        exp["in_name"] = name
        exp["in_problem"] = row[0]
        exp["in_samples"] = row[3]
        exp["in_seed"] = row[4]
        exp["modeltestTruePositives"] = row[5]
        exp["modeltestFalsePositives"] = row[6]
        exp["modeltestTrueNegatives"] = row[7]
        exp["modeltestFalseNegatives"] = row[8]
        exp["totalTime"] = row[10]
        exp.save()


if __name__ == '__main__':
    main()

