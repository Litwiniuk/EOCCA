﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MersenneTwister;
using EOCCA.Shared;

namespace EOCCA
{
    public class BallExperiment : IExperiment
    {
        private static readonly double TWOSQRT6 = 2.0 * Math.Sqrt(6.0);
        private static readonly double TWOSQRT6DPI = TWOSQRT6 * _d / Math.PI;

        private const double _d = 2.7;
        private const double _d2 = 2.0 * _d;
        private const double L = 1e12;
        private const double _ddL = _d * _d + L;

        public double FirstConstraint(int i, int k)
        {
            return i - _d2;
        }

        public double SecondConstraint(int i, int k)
        {
            return i + (TWOSQRT6DPI * (k - 1)) + _d2;
        }

        public bool Constraints(double[] x, int n, int j, int bj)
        {
            double v;
            double sum = 0;
            for (int i = 1; i <= n; i++)
            {
                v = x[i - 1] - i - ((TWOSQRT6DPI * (j - 1)) / i);
                sum += v * v;
            }

            return _ddL >= sum + L * bj;
        }

        public string GetName()
        {
            return "ball";
        }
    }
}
