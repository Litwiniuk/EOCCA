import os
from pyclustering.cluster.xmeans import xmeans, splitting_type;

from pyclustering.utils import timedcall;
import pandas as pd
import sys
from sklearn.cluster import KMeans

def template_clustering(path, k_min, k_max, tolerance = 0.025, criterion = splitting_type.BAYESIAN_INFORMATION_CRITERION, ccore = False):
    df = pd.read_csv(path, sep=',', header=None, skiprows=1)
    #print(df.values)

    kmeans = KMeans(n_clusters=k_min, random_state=0).fit(df.values)
    start_centers = kmeans.cluster_centers_

    xmeans_instance = xmeans(df.values, start_centers, k_max, tolerance, criterion, ccore);
    (ticks, _) = timedcall(xmeans_instance.process);

    clusters = xmeans_instance.get_clusters();
    centers = xmeans_instance.get_centers();

    print(len(clusters))

#print(os.path.dirname(os.path.abspath(__file__)))
path = sys.argv[1]
#path = "C:\\Users\\Stacja\\Documents\\Visual Studio 2017\\Projects\\SVDD_Algorithm\\SVDD_Algorithm\\bin\\Debug\\Data\\Train\\ball_6_2_3.csv"
k_min = int(sys.argv[2])
k_max = int(sys.argv[3])

template_clustering(path, k_min, k_max, criterion = splitting_type.BAYESIAN_INFORMATION_CRITERION);

#xmeansfit(k_min, k_max, path)
