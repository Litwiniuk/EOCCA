import sqlite3
import math
import pandas as pd
import numpy as np
from scipy import stats
from colour import Color
from sklearn.preprocessing import QuantileTransformer, Normalizer
from scipy.stats import wilcoxon
import operator


class LatexTable:
    def __init__(self):
        self.db_name = "../bin/Release/statistics.sqlite"
        self.db_conn = sqlite3.connect(self.db_name)
        self.round_num = 5
        self.round_val = 2
        # self.measures = ['objectiveFunction', 'fscore', 'matthewsCorrelationCoefficient','out_clusters', 'recall', 'precision']
        self.measures = ['objectiveFunction', 'fscore']
        pass

    def load_table(self):
        pass

    def conf_normalizer(self, conf_array):
        self.max = max(conf_array)
        self.min = min(conf_array)

    def normalize(self, val):
        return (val - self.min) / (self.max - self.min)

    def table(self, info, col_name, title, label, caption, measure, color=True):
        df = pd.read_sql_query("select * from constraints where info='" + info + "';", self.db_conn)
        confidence_interval = df.groupby(('benchmark', col_name))[measure].sem(ddof=1) * stats.norm.ppf(
            q=0.975)
        mean = df.groupby(('benchmark', col_name))[measure].mean()

        print(mean)
        print(confidence_interval)

        experiments = set()
        values = set()
        for i, v in mean.items():
            print('index: ', i, 'value: ', v)
            experiments.add(i[0])
            values.add(i[1])

        experiments = sorted(experiments)
        values = sorted(values)

        lx_table = LatexTable.generate_header(caption, label, title, len(values))
        lx_table += LatexTable.generate_title_row(values)

        rank = {}
        series = []
        result_series = {}
        for experiment in experiments:
            for std_val in values:
                series.append(round(mean[(experiment, std_val)], 5))
                val = mean[(experiment, std_val)]
                if math.isnan(val) or val is None:
                    val = 0.0
                if (std_val) in result_series.keys():
                    result_series[(std_val)].append(val)
                else:
                    result_series[(std_val)] = [val]

        self.__normalize(series)
        self.conf_normalizer(confidence_interval)

        for experiment in experiments:

            experiment_values = []
            for std_val in values:
                obj_val = round(mean[(experiment, std_val)], 5)
                experiment_values.append(obj_val)
            lx_table += LatexTable.benchmark_name_to_latex_format(experiment)
            # lx_table += name.replace('_', '\_')
            rank_dict = LatexTable.rank_values(experiment_values)

            for std_val in values:
                obj_val = round(mean[(experiment, std_val)], 5)

                if math.isnan(obj_val):
                    lx_table += " &  "
                    continue

                color = self.map_to_color(np.array(self.colours[obj_val]).flatten()[0])

                if std_val in rank.keys():
                    rank[std_val] += rank_dict[obj_val]
                else:
                    rank[std_val] = rank_dict[obj_val]

                obj_val = round(obj_val, self.round_val)
                # + \
                conf_val = self.normalize(confidence_interval[(experiment, std_val)])
                lx_table += " & " + "\cellcolor[rgb]{" + color + "} " + \
                            "{0:.2f}".format(obj_val) + self.margin(conf_val)
                #  " $\\pm$ " + str(round(confidence_interval[(experiment, std_val)], 2))
            lx_table += " \\\\ \hline \n"

        rank_line = "rank"
        best_val = 999999999
        best_key = None
        for std_val in values:
            val = rank[std_val] / len(experiments)
            if val < best_val:
                best_val = val
                best_key = (std_val)
            rank_line += " & " + "{0:.3f}".format(round(val, self.round_num))
        lx_table += rank_line + "\\\\ \hline"

        lx_table += "\n p-value"
        wilcoxon_dict = LatexTable.wilcoxon(result_series, best_key)

        for std_val in values:
            value = wilcoxon_dict[(std_val)]
            if isinstance(value, str):
                lx_table += " & " + value
            else:
                lx_table += " & " + "{0:.3f}".format(value)
        lx_table += " \\\\ \hline \n"

        lx_table += """ \n \end{tabular}}
                \end{table}"""
        print(result_series)
        # LatexTable.wilcoxon(result_series, 0)
        print(lx_table)
        return lx_table

    def table_clusters(self, info, col_name, title, label, caption, measure, color=True):
        df = pd.read_sql_query("select * from constraints where info='" + info + "';", self.db_conn)
        confidence_interval = df.groupby(('benchmark', 'clustering_method', 'in_clusters_str'))[measure].sem(
            ddof=1) * stats.norm.ppf(
            q=0.975)
        mean = df.groupby(('benchmark', 'clustering_method', 'in_clusters_str'))[measure].mean()

        print(mean)
        print(confidence_interval)

        experiments = set()
        cluster_alg = set()
        values = set()
        for i, v in mean.items():
            print('index: ', i, 'value: ', v)
            experiments.add(i[0])
            cluster_alg.add(i[1])
            values.add(i[2])

        experiments = sorted(experiments)
        values = [1, "n", "2n", "n^2"]  # sorted(values)

        header_val = list(values) + list(values)
        for i in range(len(header_val)):
            header_val[i] = "$" + str(header_val[i]) + "$"
        lx_table = LatexTable.generate_header(caption, label, title, len(values) * len(cluster_alg))
        lx_table += '\multicolumn{0}{|c|}{} & '
        for method in cluster_alg:
            lx_table += '\multicolumn{4}{c|}{\\textbf{' + str(method) + '}}'
            if method != list(cluster_alg)[len(cluster_alg) - 1]:
                lx_table += ' & '
        lx_table += '\\\\ \hline'

        lx_table += LatexTable.generate_title_row(header_val)

        rank = {}
        series = []
        result_series = {}
        for experiment in experiments:
            for method in cluster_alg:
                for cluster_in in values:
                    series.append(round(mean[(experiment, method, cluster_in)], 5))
                    val = mean[(experiment, method, cluster_in)]
                    if math.isnan(val) or val is None:
                        val = 0.0
                    if (method, cluster_in) in result_series.keys():
                        result_series[(method, cluster_in)].append(val)
                    else:
                        result_series[(method, cluster_in)] = [val]

        self.__normalize(series)
        self.conf_normalizer(confidence_interval)
        for experiment in experiments:
            experiment_values = []
            for method in cluster_alg:
                for std_val in values:
                    obj_val = round(mean[(experiment, method, std_val)], 5)
                    experiment_values.append(obj_val)
            lx_table += LatexTable.benchmark_name_to_latex_format(experiment)
            # lx_table += name.replace('_', '\_')
            rank_dict = LatexTable.rank_values(experiment_values)

            for method in cluster_alg:
                for std_val in values:

                    obj_val = round(mean[(experiment, method, std_val)], 5)

                    if math.isnan(obj_val):
                        lx_table += " &  "
                        continue

                    color = self.map_to_color(np.array(self.colours[obj_val]).flatten()[0])

                    if (method, std_val) in rank.keys():
                        rank[(method, std_val)] += rank_dict[obj_val]
                    else:
                        rank[(method, std_val)] = rank_dict[obj_val]

                    obj_val = round(obj_val, self.round_val)
                    conf_val = self.normalize(confidence_interval[(experiment, method, std_val)])
                    lx_table += " & " + "\cellcolor[rgb]{" + color + "} " + \
                                "{0:.2f}".format(obj_val) + self.margin(conf_val)
                    #  " $\\pm$ " + str(round(confidence_interval[(experiment, std_val)], 2))
            lx_table += " \\\\ \hline \n"

        rank_line = "rank"

        best_val = 999999999
        best_key = None
        for method in cluster_alg:
            for std_val in values:
                val = rank[(method, std_val)] / len(experiments)
                if val < best_val:
                    best_val = val
                    best_key = (method, std_val)
                rank_line += " & " + "{0:.3f}".format(round(val, self.round_num))
        lx_table += rank_line + "\\\\ \hline"

        lx_table += "\n p-value"
        wilcoxon_dict = LatexTable.wilcoxon(result_series, best_key)

        for method in cluster_alg:
            for std_val in values:
                value = wilcoxon_dict[(method, std_val)]
                if isinstance(value, str):
                    lx_table += " & " + value
                else:
                    lx_table += " & " + "{0:.3f}".format(value)
        lx_table += " \\\\ \hline \n"

        lx_table += """ \n \end{tabular}}
                \end{table}"""

      #  print(result_series)
      #  LatexTable.wilcoxon(result_series, best_key)
    #    print(lx_table)
        return lx_table

    @staticmethod
    def rank_values(values):
        values = sorted(values, reverse=True)
        current = 1
        result = {}
        for val in values:
            if val in result.keys():
                continue
            if values.count(val) == 1:
                result[val] = current
                current += 1
            else:
                result[val] = current + 0.5
                current += 2
        print(result)
        return result

    def __normalize(self, series):
        # series = series.applymap(lambda x: np.log(1+x))
        scaler = QuantileTransformer()
        values = np.nan_to_num(series)
        scaler.fit(values.reshape(-1, 1))
        series2 = []

        for s in series:
            if math.isinf(s) or math.isnan(s):
                series2.append(0)
                continue
            series2.append(scaler.transform(s))
            # series2.append(scaler.transform(np.array(s).reshape(-1, 1)))
        # series2 = series.apply(np.nan_to_num).applymap(lambda x: scaler.transform(x)[0][0])
        series2 = np.array(series2).flatten()
        for i in range(len(series)):
            self.colours[series[i]] = series2[i]
        return series2

    colours = {}

    def map_to_color(self, x: float) -> str:
        if x > 1:
            x = 1
        if x < 0:
            x = 0
        xp = format(1 - x, 'f')[:7]
        x = format(x, 'f')
        # return '{},{},{}'.format(x, 1 - x, 0)
        return '{},{},{}'.format(xp, x, 0)

    @staticmethod
    def generate_colors(dictz):
        result = {}
        green = Color("green")
        colors = list(green.range_to(Color("red"), len(dictz)))
        dict_sorted = sorted(dictz.items(), key=operator.itemgetter(1))
        iterator = 0
        for k, v in dict_sorted:
            result[k] = colors[iterator].hex_l.replace('#', '')
            iterator += 1
        print(result)
        return result

    @staticmethod
    def margin(val):
        if math.isnan(val):
            val = 0.0
        val *= 3
        if val > 1:
            val = 1
        val = str(format(val, 'f'))
        return '  \\begin{tikzpicture}[y=0.75em,baseline=1pt]\draw[very thick] (0,0) -- (0,' + val + ');\end{tikzpicture}'

    @staticmethod
    def generate_header(caption, label, title, cols_number):
        cols = (cols_number + 1) * '|r|'
        cols = cols.replace('||', '|')
        return """\\begin{{table}}[H]
                \centering
                \caption{0}
                \label{1}
                \\resizebox{{\\textwidth}}{{!}}{{%
                \\begin{{tabular}}{2} \hline
                \multicolumn{4}{{|c|}}{3} \\\\ \hline """.format('{' + caption + '}', '{' + label + '}',
                                                                 '{' + cols + '}', '{\\textbf{' + title + '}}',
                                                                 '{' + str(cols_number + 1) + '}') + "\n"

    @staticmethod
    def generate_title_row(values):
        result = "\\textbf{problem}"

        for value in values:
            value = "\\textbf{" + str(value) + "}"
            result += " & " + value

        result += " \\\\ \hline \n"
        return result

    def std_table(self):
        for measure in self.measures:
            t = LatexTable.table(self, "tuning_std", "std", "Standaryzacja", "std_table_" + measure,
                                 "Wpływ standaryzacji na jakość rozwiązań", measure)
            self.save_to_file("std-" + measure, t)

    def pca_table(self):
        for measure in self.measures:
            t = LatexTable.table(self, "tuning_pca", "pca", "Wpływ PCA na jakość rozwiązań", "pca_table_" + measure,
                                 "PCA", measure)
            self.save_to_file("pca-" + measure, t)

    def clusters_table(self):
        for measure in self.measures:
            t = LatexTable.table_clusters(self, "tuning_clusters", "clustering_method",
                                          "Wpływa algorytmu grupującego i liczby grup na jakość rozwiązań",
                                          "table:clusters_experiment_" + measure, "Algorytm grupujący", measure)
            self.save_to_file("clusters-" + measure, t)

    def radius_table(self):
        for measure in self.measures:
            t = LatexTable.table(self, "tuning_radius", "extraRadius", "Wpływ marginesu promienia na jakość rozwiązań",
                                 "radius_table_" + measure, "Margines promienia", measure)
            self.save_to_file("radius-" + measure, t)

    def training_points_table(self):
        for measure in self.measures:
            t = LatexTable.training_table(self, "tuning_trainset_", "trainingPointsSize",
                                          "Wpływ rozmiaru zbioru uczącego",
                                          "training_size", "table:training_simplex_" + measure, "simplex", measure,
                                          color=True)

            self.save_to_file("train_size_simpleks-" + measure, t)

            t = LatexTable.training_table(self, "tuning_trainset_", "trainingPointsSize",
                                          "Wpływ rozmiaru zbioru uczącego",
                                          "training_size", "table:training_ball_" + measure, "ball", measure,
                                          color=True)
            self.save_to_file("train_size_ball-" + measure, t)

            t = LatexTable.training_table(self, "tuning_trainset_", "trainingPointsSize",
                                          "Wpływ rozmiaru zbioru uczącego",
                                          "training_size", "table:training_cube_" + measure, "cube", measure,
                                          color=True)

            self.save_to_file("train_size_cube-" + measure, t)

    def training_table(self, info, col_name, title, label, caption, experiment_name, measure, color=True):
        df = pd.read_sql_query(
            "select * from constraints where info LIKE '%" + info + "%'" + " AND benchmark LIKE '%" + experiment_name + "%'" + ";",
            self.db_conn)
        confidence_interval = df.groupby((col_name, 'benchmark'))[measure].sem(ddof=1) * stats.norm.ppf(
            q=0.975)
        mean = df.groupby((col_name, 'benchmark'))[measure].mean()

        print(mean)
        print(confidence_interval)

        experiments = set()
        values = set()
        for i, v in mean.items():
            print('index: ', i, 'value: ', v)
            experiments.add(i[0])
            values.add(i[1])

        experiments = sorted(experiments)
        values = sorted(values)

        values_latex = list(map(LatexTable.benchmark_name_to_latex_format, values))

        lx_table = LatexTable.generate_header(caption, label, title, len(values))
        lx_table += LatexTable.generate_title_row(values_latex)

        rank = {}
        series = []

        result_series = {}
        for experiment in experiments:
            for std_val in values:
                series.append(round(mean[(experiment, std_val)], 5))
                val = mean[(experiment, std_val)]
                if math.isnan(val) or val is None:
                    val = 0.0
                if (std_val) in result_series.keys():
                    result_series[(std_val)].append(val)
                else:
                    result_series[(std_val)] = [val]

        self.__normalize(series)
        self.conf_normalizer(confidence_interval)
        for experiment in experiments:
            experiment_values = []
            for std_val in values:
                obj_val = round(mean[(experiment, std_val)], 5)
                experiment_values.append(obj_val)
            #            lx_table += LatexTable.benchmark_name_to_latex_format(experiment)
            lx_table += str(experiment)
            rank_dict = LatexTable.rank_values(experiment_values)
            for std_val in values:

                obj_val = round(mean[(experiment, std_val)], 5)

                if math.isnan(obj_val):
                    lx_table += " &  "
                    continue

                color = self.map_to_color(np.array(self.colours[obj_val]).flatten()[0])

                if std_val in rank.keys():
                    rank[std_val] += rank_dict[obj_val]
                else:
                    rank[std_val] = rank_dict[obj_val]

                obj_val = round(obj_val, self.round_val)
                conf_val = self.normalize(confidence_interval[(experiment, std_val)])
                lx_table += " & " + "\cellcolor[rgb]{" + color + "} " + \
                            "{0:.2f}".format(obj_val) + self.margin(conf_val)
                #  " $\\pm$ " + str(round(confidence_interval[(experiment, std_val)], 2))
            lx_table += " \\\\ \hline \n"

        rank_line = "rank"
        best_val = 999999999
        best_key = None

        for std_val in values:
            val = rank[(std_val)] / len(experiments)
            if val < best_val:
                best_val = val
                best_key = (std_val)
            rank_line += " & " + "{0:.3f}".format(round(val, self.round_num))
        lx_table += rank_line + "\\\\ \hline"

        lx_table += "\n p-value"
        wilcoxon_dict = LatexTable.wilcoxon(result_series, best_key)

        for std_val in values:
            value = wilcoxon_dict[(std_val)]
            if isinstance(value, str):
                lx_table += " & " + value
            else:
                lx_table += " & " + "{0:.3f}".format(value)
        lx_table += " \\\\ \hline \n"

        lx_table += """ \n \end{tabular}}
                \end{table}"""
        print(lx_table)
        return lx_table

    @staticmethod
    def save_to_file(filename, string):
        path = '../bin/Release/tables/' + filename + '.tex'
        with open(path, "w+", encoding='utf8') as f:
            f.write(string)

    @staticmethod
    def benchmark_name_to_latex_format(name):
        if type(name) != str:
            return name
        first_ = False
        result = ""
        for c in name:
            if c != '_':
                result += c
            if c == '_' and not first_:
                result += '' + c
                first_ = True
            elif c == '_' and first_:
                result += '^'
        result += '$'
        return '$' + result.title()

    @staticmethod
    def wilcoxon(data, max_index):
        result = {}
        max_series = data[max_index]
        for key, value in data.items():
            if key == max_index:
                result[key] = '---'
            else:
                result[key] = wilcoxon(max_series, data[key]).pvalue
        return result

    @staticmethod
    def print_wilcoxon(values):
        result = "\n p-value"
        for value in values:
            if isinstance(value, str):
                result += " & " + value
            else:
                result += " & " + "{0:.2f}".format(value)
        result += " \\\\ \hline \n"
        return result


table = LatexTable()

table.training_points_table()
table.std_table()
table.pca_table()
table.radius_table()
table.clusters_table()
