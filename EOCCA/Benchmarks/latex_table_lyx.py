import sqlite3
import math
import pandas as pd
import numpy as np
from scipy import stats
from colour import Color
from sklearn.preprocessing import QuantileTransformer, Normalizer
from scipy.stats import wilcoxon
import operator
import functools


class LatexTable:
    def __init__(self):
        self.db_name = "statistics.sqlite"
        self.db_conn = sqlite3.connect(self.db_name)
        self.db_conn.cursor().execute("CREATE TEMP VIEW constraintsTransformed AS SELECT IFNULL(fscore, 0.0) AS fscore,* FROM constraints")
        self.round_num = 5
        self.round_val = 2
        # self.measures = ['objectiveFunction', 'fscore', 'matthewsCorrelationCoefficient','out_clusters', 'recall', 'precision']
        self.measures = ['fscore']
        pass

    def load_table(self):
        pass

    def conf_normalizer(self, conf_array):
        self.max = max(conf_array)
        self.min = min(conf_array)

    def normalize(self, val):
        return (val - self.min) / (self.max - self.min)

    def table(self, info, col_name, title, label, caption, measure, color=True, hide_first_col=False,
              hide_table_tag=True, replace_num_with_str=False):
        df = pd.read_sql_query("select * from constraintsTransformed where info='" + info + "';", self.db_conn)
        confidence_interval = df.groupby(('benchmark', col_name))[measure].sem(ddof=1) * stats.norm.ppf(
            q=0.975)
        mean = df.groupby(('benchmark', col_name))[measure].mean()

        print(mean)
        print(confidence_interval)

        experiments = set()
        values = set()
        for i, v in mean.items():
            print('index: ', i, 'value: ', v)
            experiments.add(i[0])
            values.add(i[1])

        experiments = list(experiments)
        cmp = functools.cmp_to_key(LatexTable.cmp_experiments)
        experiments.sort(key=cmp)
        values = sorted(values)

        lx_table = LatexTable.generate_header(caption, label, title, len(values), hide_first_col, hide_table_tag,
                                              add_extra_row=True)
        lx_table += LatexTable.generate_title_row(values, hide_first_col, replace_num_with_str)

        rank = {}
        series = []
        result_series = {}
        for experiment in experiments:
            for std_val in values:
                series.append(round(mean[(experiment, std_val)], 5))
                val = mean[(experiment, std_val)]
                if math.isnan(val) or val is None:
                    val = 0.0
                if (std_val) in result_series.keys():
                    result_series[(std_val)].append(val)
                else:
                    result_series[(std_val)] = [val]

        self.__normalize(series)
        self.conf_normalizer(confidence_interval)

        for experiment in experiments:

            experiment_values = []
            for std_val in values:
                obj_val = round(mean[(experiment, std_val)], 5)
                experiment_values.append(obj_val)
            if not hide_first_col:
                lx_table += LatexTable.benchmark_name_to_latex_format(experiment)
            # lx_table += name.replace('_', '\_')
            rank_dict = LatexTable.rank_values(experiment_values)

            first_line = hide_first_col
            for std_val in values:
                obj_val = round(mean[(experiment, std_val)], 5)

                if math.isnan(obj_val):
                    lx_table += " &  "
                    continue

                color = self.map_to_color(np.array(self.colours[obj_val]).flatten()[0])

                if std_val in rank.keys():
                    rank[std_val] += rank_dict[obj_val]
                else:
                    rank[std_val] = rank_dict[obj_val]

                obj_val = round(obj_val, self.round_val)
                # + \
                conf_val = self.normalize(confidence_interval[(experiment, std_val)])

                if not first_line:
                    lx_table += " & "
                lx_table += "\cellcolor[rgb]{" + color + "} " + \
                            "{0:.2f}".format(obj_val) + self.margin(conf_val)
                #  " $\\pm$ " + str(round(confidence_interval[(experiment, std_val)], 2))
                first_line = False
            lx_table += " \\\\ \hline \n"

        if not hide_first_col:
            rank_line = "Rank"
        else:
            rank_line = ''
        best_val = 999999999
        best_key = None
        first_line = hide_first_col
        for std_val in values:
            val = rank[std_val] / len(experiments)
            if val < best_val:
                best_val = val
                best_key = (std_val)
            if not first_line:
                rank_line += " & "
            rank_line += "{0:.3f}".format(round(val, self.round_num))
            first_line = False
        lx_table += rank_line + "\\\\ \hline"

        if not hide_first_col:
            lx_table += "\n p-value"
        wilcoxon_dict = LatexTable.wilcoxon(result_series, best_key)

        first_line = hide_first_col
        for std_val in values:
            value = wilcoxon_dict[(std_val)]
            if not first_line:
                lx_table += " & "
            if isinstance(value, str):
                lx_table += value
            else:
                lx_table += "{0:.3f}".format(value)
            first_line = False
        lx_table += " \\\\ \hline \n"

        lx_table += """ \n \end{tabular}"""

        if not hide_table_tag:
            lx_table += """}\end{table}"""
        print(result_series)
        # LatexTable.wilcoxon(result_series, 0)
        print(lx_table)
        return lx_table

    def table_clusters(self, info, col_name, title, label, caption, measure, color=True, hide_first_col=False,
                       hide_table_tag=True):
        df = pd.read_sql_query("select * from constraintsTransformed where info='" + info + "';", self.db_conn)
        confidence_interval = df.groupby(('benchmark', 'clustering_method', 'in_clusters_str'))[measure].sem(
            ddof=1) * stats.norm.ppf(
            q=0.975)
        mean = df.groupby(('benchmark', 'clustering_method', 'in_clusters_str'))[measure].mean()

        print(mean)
        print(confidence_interval)

        experiments = set()
        cluster_alg = set()
        values = set()
        for i, v in mean.items():
            print('index: ', i, 'value: ', v)
            experiments.add(i[0])
            cluster_alg.add(i[1])
            values.add(i[2])

        experiments = sorted(experiments, key=functools.cmp_to_key(LatexTable.cmp_experiments))
        values = [1, "n", "2n", "n^2"]  # sorted(values)

        header_val = list(values) + list(values)
        for i in range(len(header_val)):
            header_val[i] = "$\\mathbf{" + str(header_val[i]) + "}$"
        lx_table = LatexTable.generate_header(caption, label, title, len(values) * len(cluster_alg), hide_first_col,
                                              hide_table_tag)
        if not hide_first_col:
            lx_table += '\multicolumn{0}{|c|}{} & '
        for method in cluster_alg:
            if hide_first_col:
                lx_table += '\multicolumn{4}{|c|}{\\textbf{' + LatexTable.replace_cluster_method(str(method)) + '}}'
            else:
                lx_table += '\multicolumn{4}{c|}{\\textbf{' + LatexTable.replace_cluster_method(str(method)) + '}}'
            if method != list(cluster_alg)[len(cluster_alg) - 1]:
                lx_table += ' & '
        lx_table += '\\\\ \hline'

        lx_table += LatexTable.generate_title_row(header_val, hide_first_col)

        rank = {}
        series = []
        result_series = {}
        for experiment in experiments:
            for method in cluster_alg:
                for cluster_in in values:
                    series.append(round(mean[(experiment, method, cluster_in)], 5))
                    val = mean[(experiment, method, cluster_in)]
                    if math.isnan(val) or val is None:
                        val = 0.0
                    if (method, cluster_in) in result_series.keys():
                        result_series[(method, cluster_in)].append(val)
                    else:
                        result_series[(method, cluster_in)] = [val]

        self.__normalize(series)
        self.conf_normalizer(confidence_interval)
        for experiment in experiments:
            experiment_values = []
            for method in cluster_alg:
                for std_val in values:
                    obj_val = round(mean[(experiment, method, std_val)], 5)
                    experiment_values.append(obj_val)
            if not hide_first_col:
                lx_table += LatexTable.benchmark_name_to_latex_format(experiment)
            # lx_table += name.replace('_', '\_')
            rank_dict = LatexTable.rank_values(experiment_values)
            first_line = True
            for method in cluster_alg:
                for std_val in values:

                    obj_val = round(mean[(experiment, method, std_val)], 5)

                    if math.isnan(obj_val):
                        lx_table += " &  "
                        continue

                    color = self.map_to_color(np.array(self.colours[obj_val]).flatten()[0])

                    if (method, std_val) in rank.keys():
                        rank[(method, std_val)] += rank_dict[obj_val]
                    else:
                        rank[(method, std_val)] = rank_dict[obj_val]

                    obj_val = round(obj_val, self.round_val)
                    conf_val = self.normalize(confidence_interval[(experiment, method, std_val)])
                    if not first_line:
                        lx_table += " & "
                    lx_table += "\cellcolor[rgb]{" + color + "} " + \
                                "{0:.2f}".format(obj_val) + self.margin(conf_val)
                    first_line = False
                    #  " $\\pm$ " + str(round(confidence_interval[(experiment, std_val)], 2))
            lx_table += " \\\\ \hline \n"

        if not hide_first_col:
            rank_line = "Rank"
        else:
            rank_line = ''

        best_val = 999999999
        best_key = None
        first_line = hide_first_col
        for method in cluster_alg:

            for std_val in values:
                val = rank[(method, std_val)] / len(experiments)
                if val < best_val:
                    best_val = val
                    best_key = (method, std_val)

                if not first_line:
                    rank_line += " & "
                rank_line += "{0:.3f}".format(round(val, self.round_num))
                first_line = False
        lx_table += rank_line + "\\\\ \hline "

        if not hide_first_col:
            lx_table += "\n p-value"
        wilcoxon_dict = LatexTable.wilcoxon(result_series, best_key)

        first_line = hide_first_col
        for method in cluster_alg:

            for std_val in values:
                value = wilcoxon_dict[(method, std_val)]
                if not first_line:
                    lx_table += " & "
                if isinstance(value, str):
                    lx_table += value
                else:
                    lx_table += "{0:.3f}".format(value)
                first_line = False
        lx_table += " \\\\ \hline \n"

        lx_table += """ \n \end{tabular}"""

        if not hide_table_tag:
            lx_table += """}\end{table}"""

        #  print(result_series)
        #  LatexTable.wilcoxon(result_series, best_key)
        #    print(lx_table)
        return lx_table

    @staticmethod
    def rank_values(values, descending=True):
        values = sorted(values, reverse=descending)
        current = 1
        result = {}
        for val in values:
            if val in result.keys():
                continue
            if values.count(val) == 1:
                result[val] = current
                current += 1
            else:
                result[val] = current + 0.5
                current += 2
        print(result)
        return result

    def __normalize(self, series):
        # series = series.applymap(lambda x: np.log(1+x))
        scaler = QuantileTransformer()
        values = np.nan_to_num(series)
        scaler.fit(values.reshape(-1, 1))
        series2 = []

        for s in series:
            if math.isinf(s) or math.isnan(s):
                series2.append(0)
                continue
            series2.append(scaler.transform([[s]]))
            # series2.append(scaler.transform(np.array(s).reshape(-1, 1)))
        # series2 = series.apply(np.nan_to_num).applymap(lambda x: scaler.transform(x)[0][0])
        series2 = np.array(series2).flatten()
        for i in range(len(series)):
            self.colours[series[i]] = series2[i]
        return series2

    colours = {}

    def map_to_color(self, x: float, reverse_color: bool = False) -> str:
        if x > 1:
            x = 1
        if x < 0:
            x = 0
        xp = 1 - x # format(1 - x, 'f')[:7]
        # x = format(x, 'f')
        # return '{},{},{}'.format(x, 1 - x, 0)
        if reverse_color:
            return '{},{},{}'.format(x*0.7+0.3, xp*0.7+0.3, 0.3)
        return '{},{},{}'.format(xp*0.7+0.3, x*0.7+0.3, 0.3)

    @staticmethod
    def generate_colors(dictz):
        result = {}
        green = Color("green")
        red = Color("red")
        colors = list(green.range_to(red, len(dictz)))
        dict_sorted = sorted(dictz.items(), key=operator.itemgetter(1))
        iterator = 0
        for k, v in dict_sorted:
            result[k] = colors[iterator].hex_l.replace('#', '')
            iterator += 1
        print(result)
        return result

    @staticmethod
    def margin(val):
        if math.isnan(val):
            val = 0.0
        # val *= 3
        val /= 0.3
        if val > 1:
            val = 1
        val = str(format(val, 'f'))
        return '  \\begin{tikzpicture}[y=0.75em,baseline=1pt]\draw[very thick] (0,0) -- (0,' + val + ');\end{tikzpicture}'

    @staticmethod
    def generate_header(caption, label, title, cols_number, hide_first_col=False, hide_table_tag=True,
                        add_extra_row=False):
        if hide_first_col:
            cols_number -= 1
        cols = (cols_number + 1) * '|c|'
        cols = cols.replace('||', '|')
        table_tag = ""
        if not hide_table_tag:
            table_tag += """
            \\begin{{table}}[H]
                \centering
                \caption{0}
                \label{1}
                \\resizebox{{\\textwidth}}{{!}}{{%""".format('{' + caption + '}', '{' + label + '}')

        extra_row = ""
        if add_extra_row:
            extra_row += """\multicolumn{1}{{|c|}}{0} \\\\ """.format('{}',
                                                                      '{' + str(cols_number + 1) + '}')
        return """{0}
                \\begin{{tabular}}{1} \hline
                {4}
                \multicolumn{3}{{|c|}}{2} \\\\ \hline """.format(table_tag,
                                                                 '{' + cols + '}', '{\\textbf{' + title + '}}',
                                                                 '{' + str(cols_number + 1) + '}', extra_row) + "\n"

    @staticmethod
    def generate_title_row(values, hide_first_col=False, replace_num_with_str=False):
        if not hide_first_col:
            result = "\\textbf{Problem}"
        else:
            result = ''

        hide_first_line = hide_first_col
        for value in values:
            if isinstance(value, float):
                str_val = "{0:.2f}".format(value)
            else:
                if replace_num_with_str:
                    if value == 1:
                        str_val = "on"
                    elif value == 0:
                        str_val = 'off'
                    else:
                        str_val = str(value)
                else:
                    str_val = str(value)
            value = "\\textbf{" + str_val + "}"
            if not hide_first_line:
                result += " & "
            result += value
            hide_first_line = False

        result += " \\\\ \hline \n"
        return result

    @staticmethod
    def replace_cluster_method(method: str):
        return method.lower().replace('kmeans', 'k-means').replace('pyxmeans', 'x-means').replace('xmeans', 'x-means')

    def std_table(self):
        for measure in self.measures:
            t = LatexTable.table(self, "tuning_std", "std", "Standaryzacja", "std_table_" + measure,
                                 "Wpływ standaryzacji na jakość rozwiązań", measure)
            self.save_to_file("std-" + measure, t)

    def pca_table(self):
        for measure in self.measures:
            t = LatexTable.table(self, "tuning_pca", "pca", "Wpływ PCA na jakość rozwiązań", "pca_table_" + measure,
                                 "PCA", measure, hide_first_col=True)
            self.save_to_file("pca-" + measure, t)

    def clusters_table(self):
        for measure in self.measures:
            t = LatexTable.table_clusters(self, "tuning_clusters", "clustering_method",
                                          "Wpływa algorytmu grupującego i liczby grup na jakość rozwiązań",
                                          "table:clusters_experiment_" + measure, "Algorytm grupujący", measure,
                                          hide_first_col=True)
            print(t)
            self.save_to_file("clusters-" + measure, t)

    def radius_table(self):
        for measure in self.measures:
            t = LatexTable.table(self, "tuning_radius", "extraRadius", "Wpływ marginesu promienia na jakość rozwiązań",
                                 "radius_table_" + measure, "Margines promienia", measure)
            self.save_to_file("radius-" + measure, t)

    def training_points_table(self):
        for measure in self.measures:
            t = LatexTable.training_table(self, "tuning_trainset_", "trainingPointsSize",
                                          "zbior uczacy",
                                          "training_size", "table:training_simplex_" + measure, "simplex", measure,
                                          color=True, hide_first_col=True)

            # self.save_to_file("train_size_simpleks-" + measure, t)
            #
            # t = LatexTable.training_table(self, "tuning_trainset_", "trainingPointsSize",
            #                               "Wpływ rozmiaru zbioru uczącego",
            #                               "training_size", "table:training_ball_" + measure, "ball", measure,
            #                               color=True)
            # self.save_to_file("train_size_ball-" + measure, t)
            #
            # t = LatexTable.training_table(self, "tuning_trainset_", "trainingPointsSize",
            #                               "Wpływ rozmiaru zbioru uczącego",
            #                               "training_size", "table:training_cube_" + measure, "cube", measure,
            #                               color=True)

            self.save_to_file("train_size_cube-" + measure, t)

    def training_table(self, info, col_name, title, label, caption, experiment_name, measure, color=True,
                       hide_first_col=False,
                       hide_table_tag=True, reverse_color: bool = False):
        df = pd.read_sql_query(
            "select * from constraintsTransformed where info LIKE '%" + info + "%'" + ";",
            self.db_conn)
        confidence_interval = df.groupby((col_name, 'benchmark'))[measure].sem(ddof=1) * stats.norm.ppf(
            q=0.975)
        mean = df.groupby((col_name, 'benchmark'))[measure].mean()
        mean.to_csv("tables/" + experiment_name + "_" + measure + ".csv")
        print(mean)
        print(confidence_interval)

        experiments = set()
        values = set()
        for i, v in mean.items():
            print('index: ', i, 'value: ', v)
            experiments.add(i[1])
            values.add(i[0])

        experiments = list(experiments)
        cmp = functools.cmp_to_key(LatexTable.cmp_experiments)
        experiments.sort(key=cmp)
        values = sorted(values)

        values_latex = list(map(LatexTable.benchmark_name_to_latex_format, values))

        lx_table = LatexTable.generate_header(caption, label, title, len(values), hide_first_col, hide_table_tag)
        lx_table += LatexTable.generate_title_row(values_latex, hide_first_col)

        rank = {}
        series = []

        result_series = {}
        for experiment in experiments:
            for std_val in values:
                series.append(round(mean[(std_val, experiment)], 5))
                val = mean[(std_val, experiment)]
                if math.isnan(val) or val is None:
                    val = 0.0
                if (std_val) in result_series.keys():
                    result_series[(std_val)].append(val)
                else:
                    result_series[(std_val)] = [val]

        self.__normalize(series)
        self.conf_normalizer(confidence_interval)
        for experiment in experiments:
            experiment_values = []
            for std_val in values:
                obj_val = round(mean[(std_val, experiment)], 5)
                experiment_values.append(obj_val)
            if not hide_first_col:
                lx_table += LatexTable.benchmark_name_to_latex_format(experiment)
            rank_dict = LatexTable.rank_values(experiment_values, not reverse_color)
            first_line = hide_first_col
            for std_val in values:

                obj_val = round(mean[(std_val, experiment)], 5)

                if math.isnan(obj_val):
                    lx_table += " &  "
                    continue

                color = self.map_to_color(np.array(self.colours[obj_val]).flatten()[0], reverse_color)

                if std_val in rank.keys():
                    rank[std_val] += rank_dict[obj_val]
                else:
                    rank[std_val] = rank_dict[obj_val]

                obj_val = round(obj_val, self.round_val)
                conf_val = self.normalize(confidence_interval[(std_val, experiment)])
                if not first_line:
                    lx_table += " & "
                lx_table += "\cellcolor[rgb]{" + color + "} " + \
                            "{0:.2f}".format(obj_val) + self.margin(conf_val)
                first_line = False
                #  " $\\pm$ " + str(round(confidence_interval[(experiment, std_val)], 2))
            lx_table += " \\\\ \hline \n"

        if not hide_first_col:
            rank_line = "Rank"
        else:
            rank_line = ""
        best_val = 999999999
        best_key = None

        first_line = hide_first_col
        for std_val in values:
            val = rank[(std_val)] / len(experiments)
            if val < best_val:
                best_val = val
                best_key = (std_val)
            if not first_line:
                rank_line += " & "
            rank_line += "{0:.3f}".format(round(val, self.round_num))
            first_line = False
        lx_table += rank_line + "\\\\ \hline \n"

        if not hide_first_col:
            lx_table += "\n p-value"
        wilcoxon_dict = LatexTable.wilcoxon(result_series, best_key)

        first_line = hide_first_col
        for std_val in values:
            value = wilcoxon_dict[(std_val)]
            if not first_line:
                lx_table += " & "
            if isinstance(value, str):
                lx_table += value
            else:
                lx_table += "{0:.3f}".format(value)
            first_line = False
        lx_table += " \\\\ \hline \n"

        lx_table += """ \n \end{tabular}"""

        if not hide_table_tag:
            lx_table += """}\end{table}"""
        print(lx_table)
        return lx_table

    @staticmethod
    def save_to_file(filename, string):
        path = 'tables/' + filename + '.tex'
        with open(path, "w+", encoding='utf8') as f:
            f.write(string)

    @staticmethod
    def benchmark_name_to_latex_format(name):
        if type(name) != str:
            return name
        first_ = False
        result = ""
        for c in name:
            if c != '_':
                result += c
            if c == '_' and not first_:
                result += '' + c
                first_ = True
            elif c == '_' and first_:
                result += '^'
        result += '$'
        return '$' + result.title()

    @staticmethod
    def wilcoxon(data, max_index):
        result = {}
        max_series = data[max_index]
        for key, value in data.items():
            if key == max_index:
                result[key] = '--- '
            else:
                result[key] = wilcoxon(max_series, data[key]).pvalue
        return result

    @staticmethod
    def print_wilcoxon(values):
        result = "\n p-value"
        for value in values:
            if isinstance(value, str):
                result += " & " + value
            else:
                result += " & " + "{0:.2f}".format(value)
        result += " \\\\ \hline \n"
        return result

    def generate_first_lyx_file(self):
        for measure in self.measures:
            table_std = LatexTable.table(self, "tuning_std", "std", "(a) Standarization", "std_table_" + measure,
                                         "", measure, hide_first_col=False, hide_table_tag=True,
                                         replace_num_with_str=True)

            table_pca = LatexTable.table(self, "tuning_pca", "pca", "(b) PCA", "pca_table_" + measure,
                                         "", measure, hide_first_col=True, hide_table_tag=True,
                                         replace_num_with_str=True)

            table_clusters = LatexTable.table_clusters(self, "tuning_clusters", "clustering_method",
                                                       "(c) Partitioning algorithm / k\_{min}",
                                                       "table:clusters_experiment_" + measure,
                                                       "(c) Partitioning algorithm / k\_{min}", measure,
                                                       hide_first_col=True)

            table_radius = LatexTable.table(self, "tuning_radius", "extraRadius", "(d) m",
                                            "radius_table_" + measure, "(d) m", measure, hide_first_col=True,
                                            hide_table_tag=True)

            print('\n\n===============================\n\n')

            lyx_1 = "\\begin{minipage}{0.70in}\scalebox{0.25}{" + table_std + "}\n\end{minipage} \n"

            lyx_1 += "\\begin{minipage}{0.50in}\scalebox{0.25}{" + table_pca + "}\n\end{minipage} \n"

            lyx_1 += "\\begin{minipage}{1.85in}\scalebox{0.25}{" + table_clusters + "}\n\end{minipage} \n"

            lyx_1 += "\\begin{minipage}{0.70in}\scalebox{0.25}{" + table_radius + "}\n\end{minipage} \n"

            print(lyx_1)
            self.save_to_file("tab-1-std", table_std)
            self.save_to_file("tab-1-pca", table_pca)
            self.save_to_file("tab-1-clusters", table_clusters)
            self.save_to_file("tab-1-radius", table_radius)

            return

    def generate_second_lyx_file(self):
        # experiments = ['simplex', 'ball', 'cube']
        # experiments = ['simplex']

        for measure in self.measures:
            result = ''
            # for experiment in experiments:
            experiment= 'simplex-ball-cube'
            table_experiment = LatexTable.training_table(self, "tuning_trainset_", "trainingPointsSize",
                                                         "(a) $F_{1}$-score",
                                                         "training_size",
                                                         "table:training_" + experiment + "_" + measure, experiment,
                                                         measure,
                                                         color=True, hide_first_col=False).replace('Problem',
                                                                                                   'Prob./$|X|$')

            table_time = LatexTable.training_table(self, "tuning_trainset_", "trainingPointsSize",
                                                   "(b) CPU time [s]",
                                                   "training_size",
                                                   "table:training_" + experiment + "_" + 'time', experiment,
                                                   'time',
                                                   color=True, hide_first_col=True, reverse_color=True)

            print('\n\n===============================\n\n')

            lyx_2 = "\\begin{minipage}{1.2in}\scalebox{0.25}{" + table_experiment + "}\n\end{minipage} \n"

            lyx_2 += "\\begin{minipage}{0.50in}\scalebox{0.25}{" + table_time + "}\n\end{minipage} \n"

            print(lyx_2)
            result += lyx_2
            self.save_to_file("tab-2-f1", table_experiment)
            self.save_to_file("tab-2-time", table_time)
            #self.save_to_file("training-" + measure, result)
        return

    @staticmethod
    def cmp_experiments(a, b):
        a_exp = a[:len(a) - 4]
        a_n = a[len(a) - 3:len(a) - 2]
        a_k = a[len(a) - 1:len(a)]

        b_exp = b[:len(b) - 4]
        b_n = b[len(b) - 3:len(b) - 2]
        b_k = b[len(b) - 1:len(b)]
        if a_exp > b_exp:
            return 1
        elif a_exp < b_exp:
            return -1

        if a_k > b_k:
            return 1
        elif a_k < b_k:
            return -1

        if a_n > b_n:
            return 1
        elif a_n < b_n:
            return -1
        return 0


table = LatexTable()
table.generate_first_lyx_file()
table.generate_second_lyx_file()

# table.training_points_table()
# table.std_table()
# table.pca_table()
# table.radius_table()
# table.clusters_table()
