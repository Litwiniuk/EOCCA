﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MersenneTwister;
using EOCCA.Shared;

namespace EOCCA
{
    public class SimplexExperiment : IExperiment
    {
        private static readonly double TANPI12 = Math.Tan(Math.PI / 12.0);
        private static readonly double CTANPI12 = 1.0 / Math.Tan(Math.PI / 12.0);

        private const double _d = 2.7;
        private const double L = 1e12;

        public double FirstConstraint(int i, int k)
        {
            //return -1;
            return 0;
        }

        public double SecondConstraint(int i, int k)
        {
            //return 2 * k + _d;
            return Math.Min(k, 2) * _d;
        }

        public bool Constraints(double[] x, int n, int j, int bj)
        {
            double sumxi = 0.0;
            for (int i = 0; i < n; i++)
            {
                for (int l = i + 1; l < n; ++l)
                {
                    if (!(x[i] * CTANPI12 - x[l] * TANPI12 - L * bj >= 2 * j - 2 - L &&
                        x[l] * CTANPI12 - x[i] * TANPI12 - L * bj >= 2 * j - 2 - L))
                    {
                        return false;
                    }
                }

                sumxi += x[i];
            }

            return sumxi + L * bj <= j * _d + L;
        }

        public string GetName()
        {
            return "simplex";
        }
    }
}
