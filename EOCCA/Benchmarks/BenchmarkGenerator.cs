﻿using MersenneTwister;
using EOCCA.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EOCCA.Benchmarks
{
    public class BenchmarkGenerator
    {
        private IExperiment _experiment;
        private int k_min;
        private int n;
        private int[] B;
        private Random randomGenerator;

        public BenchmarkGenerator(IExperiment experiment, int k_min, int n, int[] B, int seed)
        {
            this._experiment = experiment;
            this.k_min = k_min;
            this.n = n;
            this.B = B;
            this.randomGenerator = Randoms.Create(seed, RandomType.WellBalanced);
        }

        public bool[] GetLabels(double[][] dataset)
        {
            var result = new bool[dataset.Length];
            for (int i = 0; i < dataset.Length; ++i)
            {
                result[i] = CheckConstraint(dataset[i]);
            }
            return result;
        }

        public double[][] GeneratePointsSet(int pointsNumber, bool onlyPositive = false)
        {
            var result = new double[pointsNumber][];

            for (int pointCounter = 0; pointCounter < pointsNumber; pointCounter++)
            {
                var singleRow = new double[n];
                for (int i = 1; i <= n; i++)
                {
                    var x = _experiment.FirstConstraint(i, k_min);
                    var y = _experiment.SecondConstraint(i, k_min);
                    singleRow[i - 1] = randomGenerator.NextDouble(x, y);
                }

                if (onlyPositive && !CheckConstraint(singleRow))
                {
                    pointCounter--;
                }
                else
                {
                    result[pointCounter] = singleRow;
                }
            }

            return result;
        }

        public bool CheckConstraint(double[] singleRow)
        {
            for (int j = 1; j <= k_min; j++)
            {
                if (_experiment.Constraints(singleRow, n, j, B[j - 1]))
                {
                    return true;
                }
            }
            return false;
        }

        public List<double[]> GetTrainingSet(List<double[]> pointSet)
        {
            var result = new List<double[]>();
            foreach (var singleRow in pointSet)
            {
                if (CheckConstraint(singleRow))
                {
                    result.Add(singleRow);
                }
            }
            return result;
        }
    }
}

