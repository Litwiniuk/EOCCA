﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EOCCA
{
    public interface IExperiment
    {
        string GetName();

        double FirstConstraint(int i, int k);

        double SecondConstraint(int i, int k);

        bool Constraints(double[] x, int n, int j, int bj);
    }
}
