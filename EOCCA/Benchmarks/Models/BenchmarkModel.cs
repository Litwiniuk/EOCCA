﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EOCCA.Benchmarks.Models
{
    public class BenchmarkModel
    {
        public List<List<double>> TrainingSet;
        public List<List<double>> TestSet;

        public BenchmarkModel()
        {

        }
    }
}
