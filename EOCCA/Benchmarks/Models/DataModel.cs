﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace EOCCA.Benchmarks.Models
{
    public class DataModel
    {
        public double[][] Dataset;
        public bool[] Labels;

        public int N;
        public int K;
        public int Seed;
        public string FileName;

        public DataModel()
        {

        }

        public DataModel(double[][] dataset, bool[] labels)
        {
            this.Dataset = dataset;
            this.Labels = labels;
        }

        public DataModel(List<List<double>> dataset, List<bool> labels)
        {
            this.Dataset = dataset.Select(x => x.ToArray()).ToArray();
            this.Labels = labels.ToArray();
        }
    }
}