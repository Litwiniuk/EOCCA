﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EOCCA.Benchmarks.Models
{
    public class EllipseModel
    {
        public double[] Centroids;
        public double[] Radiuses;

        public EllipseModel()
        {

        }

        public EllipseModel(double[] centroids, double[] radiuses)
        {
            this.Centroids = centroids;
            this.Radiuses = radiuses;
        }
    }
}
