﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MersenneTwister;
using EOCCA.Shared;

namespace EOCCA
{
    public class StandarizedBallExperiment: IExperiment
    {
        private static float _d = 2.7f;
        private static int L = Int32.MaxValue / 2;

        public double FirstConstraint(int i, int k)
        {
            return i - 2 * _d;
        }

        public double SecondConstraint(int i, int k)
        {
            return i + ((2 * Math.Sqrt(6) * (k-1) *  _d )/Math.PI) + 2 * _d;
        }

        public bool Constraints(double[] x, int n, int j, int bj)
        {
            double sum = 0;
            for (int i = 0; i < n; i++)
            {
                sum += Math.Pow(x[i], 2); 
            }

            return sum <= Math.Pow((4.0/Math.PI), n);
        }

        public string GetName()
        {
            return "standarized_ball";
        }
    }
}
