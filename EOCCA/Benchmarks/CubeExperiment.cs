﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MersenneTwister;
using EOCCA.Shared;

namespace EOCCA
{
    public class CubeExperiment : IExperiment
    {
        private const double _d = 2.7;
        private const double L = 1e12;

        public double FirstConstraint(int i, int k)
        {
            return i - i * k * _d;
        }

        public double SecondConstraint(int i, int k)
        {
            return i + 2 * i * k * _d;
        }

        public bool Constraints(double[] x, int n, int j, int bj)
        {
            for (int i = 1; i <= n; i++)
            {
                if ((x[i - 1] - L * bj > i * j - L && x[i - 1] + bj * L < i * j + i * _d + L))
                {
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        public string GetName()
        {
            return "cube";
        }
    }
}
