import matplotlib.pyplot as plt
import matplotlib.patches as ptch
import matplotlib.pyplot as plt
import numpy.random as rnd

from matplotlib.patches import Ellipse

from mpl_toolkits.mplot3d import Axes3D

import json


# a = [[1, 2], [3, 3], [4, 4], [5, 2]]
#
# plt.plot(*zip(*a), marker='o', color='r', ls='')
# plt.show()
#
# fig,ax = plt.subplots(1)
#
# # Display the image
# ax.imshow(im)
#
# # Create a Rectangle patch
# rect = patches.Rectangle((50,100),40,30,linewidth=1,edgecolor='r',facecolor='none')
#
# # Add the patch to the Axes
# ax.add_patch(rect)
#
# plt.show()

class SVDDVisualization:
    def ___init__(self):
        pass

    def draw_testset(self):
        data = json.load(open('Predict.json'))

        test_set = data['Dataset']

        positive = []
        negative = []

        iterator = 0
        for single_y in data['Labels']:
            if single_y:
                positive.append(test_set[iterator])
            else:
                negative.append(test_set[iterator])
            iterator += 1

        test_first_coord_positive = [item[0] for item in positive]
        test_second_coord_positive = [item[1] for item in positive]

        test_first_coord_negative = [item[0] for item in negative]
        test_second_coord_negative = [item[1] for item in negative]
        
        fig = plt.figure()
        ax = None

        _2d = len(test_set[0]) == 2

        if _2d:
            ax = fig.add_subplot(111)
            ax.scatter(x=test_first_coord_positive, y=test_second_coord_positive, s=10, c='g', marker="s", label='positive')
            ax.scatter(x=test_first_coord_negative, y=test_second_coord_negative, s=10, c='r', marker="s", label='negative')

        else:
            ax = Axes3D(fig)
            test_third_coord_positive = [item[2] for item in positive]

            test_third_coord_negative = [item[2] for item in negative]

            ax.scatter(test_first_coord_positive, test_second_coord_positive, test_third_coord_positive, s=10, c='g', marker="s", label='positive')
            ax.scatter(test_first_coord_negative, test_second_coord_negative, test_third_coord_negative, s=3, c='r', marker="s", label='negative')

        plt.legend(loc='upper left');
        plt.show()

    def draw_trainset(self):
        data = json.load(open('Training.json'))

        training_set = data['TrainingSet']
        training_first_coord = [item[0] for item in training_set]
        training_second_coord = [item[1] for item in training_set]

        fig = plt.figure()

        _2d = len(training_set[0]) == 2

        if _2d:
            ax = fig.add_subplot(111)
            self.draw_ellipses(ax)
            ax.scatter(x=training_first_coord, y=training_second_coord, s=10, c='r', marker="o", label='training')
        else:
            ax = Axes3D(fig)
            training_third_coord = [item[2] for item in training_set]

            ax.scatter(training_first_coord, training_second_coord, training_third_coord, s=10, c='r', marker="o", label='training')

        plt.legend(loc='upper left');
        plt.show()

    def draw_ellipses(self, ax):
        data = json.load(open('ellipses.json'))
        for ellipse in data:
            centroids = ellipse["Centroids"]
            radiuses = ellipse["Radiuses"]

            print(ellipse)
            ells = Ellipse(xy=[centroids[0], centroids[1]], width=2 * radiuses[0], height=2 * radiuses[1])
            ells.set_alpha(0.3)

            ax.add_artist(ells)


svdd = SVDDVisualization()
svdd.draw_trainset()
svdd.draw_testset()
