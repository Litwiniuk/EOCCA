﻿import subprocess
import os


class SlurmPool:
    def __init__(self):
        try:
            os.makedirs("./scripts")
        except:
            None
        self.run = open("./run.sh", "w")
        self.run.write("#!/bin/bash\n")
        self.run.write("export LD_LIBRARY_PATH=./gurobi702/linux64/lib/\n")

    def execute(self, cmd, arguments, script_filename):
        sbatch = open("./scripts/%s.sh" % script_filename, "w")
        sbatch.write("#!/bin/bash\n")
        sbatch.write("#SBATCH -p idss-student\n")
        sbatch.write("#SBATCH -c 1 --mem=1475\n")
        sbatch.write("#SBATCH -t 8:00:00\n")
        sbatch.write("#SBATCH -Q\n")
        sbatch.write("date\n")
        sbatch.write("hostname\n")
        sbatch.write("echo %s %s\n" % (cmd, arguments))
        sbatch.write("srun %s %s && srun rm \"./scripts/%s.sh\"\n" % (cmd, arguments, script_filename))
        sbatch.close()

        self.run.write("sbatch \"./scripts/%s.sh\"\n" % script_filename)

    def close(self):
        self.run.close()

        ps = subprocess.Popen(["/bin/sh", "./run.sh"])
        ps.wait()


def main():

    pool = SlurmPool()

    total_runs = 0
    executed_runs = 0

    files_path = "./Data/Train"

    params = {
        "path": "./Data",
        "mode": "by_file",  # by_args
        "seed_min": 0,
        "seed_max": 0
    }
    files = os.listdir(files_path)

    for file in files:

        total_runs += 1

        executed_runs += 1
        params["file_path"] = files_path + "/" + file
        cmd = 'EOCCA.exe "%(mode)s" "%(path)s" %(seed_min)d  %(seed_max)s  "%(file_path)s" ' % params
        log = file
        pool.execute("mono", cmd, log)

    print("Executing runs: %d Completed runs: %d Total runs: %d" % (executed_runs, total_runs - executed_runs, total_runs))
    pool.close()

if __name__ == '__main__':
    main()
