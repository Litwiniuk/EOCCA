﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EOCCA.Shared
{
    public enum ClusteringMethods
    {
        KMEANS,
        XMEANS,
        PyXMEANS
    }
}
