﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace EOCCA.Shared
{
    public class Ibex
    {
        public static void ExportIbex(string[] constraints, string filename)
        {
            string[] variables = GetVariables(constraints);

            StringBuilder content = new StringBuilder();
            content.AppendLine("Variables");
            content.AppendLine($"  {string.Join(",", variables)};");
            content.AppendLine();
            content.AppendLine("Constraints");

            foreach(string constraint in constraints)
            {
                content.AppendLine($"  {constraint};");
            }

            content.AppendLine("end");

            System.IO.File.WriteAllText(filename, content.ToString());
        }

        private static string[] GetVariables(string[] constraints)
        {
            HashSet<string> vars = new HashSet<string>();

            foreach(string constraint in constraints)
            {
                MatchCollection matchColection = Regex.Matches(constraint, @"\bx(\d)+");
                foreach (Match match in matchColection)
                {
                    vars.Add(match.Value);
                }
            }
            return vars.ToArray();
        }
    }
}