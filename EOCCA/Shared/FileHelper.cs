﻿using EOCCA.Benchmarks.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace EOCCA.Shared
{
    public class FileHelper
    {
        public static void SaveJsonToFile(string filename, object objToSerialize)
        {
            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(objToSerialize);
            System.IO.File.WriteAllText($"{filename}.json", jsonString);
        }

        public static void SaveDatasetToCSV<T>(T[][] dataset, string fileName, bool headers = true, bool[] labels = null, bool overwrite = true)
        {
            int iterator = 0;

            if (overwrite && File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            CultureInfo.CurrentCulture = CultureInfo.InvariantCulture;
            Directory.CreateDirectory(Path.GetDirectoryName(fileName));

            using (StreamWriter file = new StreamWriter(fileName))
            {
                foreach (T[] array in dataset)
                {
                    if (headers)
                    {
                        for (int i = 0; i < array.Length; i++)
                        {
                            file.Write("x" + i + (i != array.Length - 1 ? "," : ""));
                        }
                        if (labels != null)
                        {
                            file.Write(",predict");
                        }
                        headers = false;
                        file.Write(Environment.NewLine);
                    }

                    file.Write(string.Join(",", array));

                    if (labels != null)
                    {
                        file.Write("," + Convert.ToInt32(labels[iterator]));
                    }
                    iterator++;
                    file.Write(Environment.NewLine);
                }
            }
        }



        public static DataModel[] ReadSet(string path)
        {
            List<DataModel> result = new List<DataModel>();

            string[] filesPath = Directory.GetFiles(path);

            foreach (string filePath in filesPath)
            {
                DataModel model = ReadDataSetFromCSV(filePath);
  
                result.Add(model);
            }

            return result.ToArray();
        }

        public static DataModel ReadDataSetFromCSV(string path)
        {
            string[] predictColName = { "predict" , "y"};
            List<List<double>> dataset = new List<List<double>>();
            List<bool> labels = new List<bool>();
            bool isLabels = false;
            bool header = true;
            using (StreamReader sr = new StreamReader(path))
            {
                string strResult = sr.ReadToEnd();
                string[] result = strResult.Split(new string[] { Environment.NewLine, "\n", "\r" }, StringSplitOptions.None);
                if (header)
                {
                    string[] separateResult = result[0].Split(',');
                    int number;
                    bool parseResult = Int32.TryParse(separateResult.Last(), out number);
                    if (!parseResult)
                    {
                        isLabels = string.Equals(separateResult.Last(), predictColName[0], StringComparison.OrdinalIgnoreCase) || string.Equals(separateResult.Last(), predictColName[1], StringComparison.OrdinalIgnoreCase);
                    }
                    header = false;
                }
                
                for (int i = 1; i < result.Length; i++)
                {
                    List<double> row = new List<double>();
                    if (result[i] == string.Empty)
                    {
                        continue;
                    }

                    string[] separateResult = result[i].Split(',');

                    for (int j = 0; j < separateResult.Length; j++)
                    {
                        if (isLabels && j == separateResult.Length - 1)
                        {
                            int val = Convert.ToInt32(separateResult[j]);
                            labels.Add(Convert.ToBoolean(val));
                            break;
                        }
                        row.Add(Convert.ToDouble(separateResult[j]));
                    }
                    dataset.Add(row);
                }
            }

            DataModel model = new DataModel(dataset, labels);

            string name = Path.GetFileName(path);

            try
            {
                string[] nameParts = name.Split('_');
                model.N = Convert.ToInt32(nameParts[1]);
                model.K = Convert.ToInt32(nameParts[2]);
                model.Seed = Convert.ToInt32(nameParts[3].Split('.')[0]);
                model.FileName = name;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return model;
        }
    }
}
