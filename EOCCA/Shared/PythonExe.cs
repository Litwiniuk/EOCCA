﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace EOCCA.Shared
{
    public static class PythonExe
    {
        public static double TotalProcessorTime { get; private set; }

        public static string runCmd(string cmd, string args="")
        {
            string result = string.Empty;

            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = ConfigurationSettings.AppSettings["PythonPath"];
            start.Arguments = string.Format("-O {0} {1}", cmd, args);
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            using (Process process = Process.Start(start))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    result = reader.ReadToEnd();
                    Console.Write(result);
                }

                process.Refresh();
                PythonExe.TotalProcessorTime += process.TotalProcessorTime.TotalSeconds;
            }

            return result;
        }
    }
}