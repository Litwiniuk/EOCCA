﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace EOCCA.Shared
{
	public class Standarization
	{
		private double[] _means;
		private double[] _standardDeviations;

		public Standarization(double[][] data, Standarization baseStd = null)
		{
			Mean(data);
			StandardDeviations(data);
			if (baseStd != null)
			{
				for (var i = 0; i < this._standardDeviations.Length; ++i)
				{
					if (!(this._standardDeviations[i] >= 1E-6))
					{
						this._standardDeviations[i] = baseStd._standardDeviations[i];
					}
				}
			}
		}

		public double[][] Transform(double[][] data)
		{
			int size = data[0].Length;
			double[][] transformedData = new double[data.Length][];
			for (int i = 0; i < size; i++)
			{
				for (int j = 0; j < data.Length; j++)
				{
                    if (transformedData[j] == null)
                        transformedData[j] = new double[size];
					/*if (_standardDeviations[i] == 0)
					{
						transformedData[j][i] = 0;
						continue;
					}*/
					transformedData[j][i] = (data[j][i] - _means[i]) / _standardDeviations[i];
				}
			}
            Debug.Assert(data.Length == transformedData.Length && data[0].Length == transformedData[0].Length);
			return transformedData;
		}

		public double[] Mean(double[][] data)
		{
			int size = data[0].Length;

			_means = new double[size];

			for (int i = 0; i < size; i++)
			{
				double counter = 0;
				double value = 0;

				for (int j = 0; j < data.GetLength(0); j++)
				{
					value += data[j][i];
					counter++;
				}
				_means[i] = value / counter;
			}
			return _means;
		}

		public double[] StandardDeviations(double[][] data)
		{
			int size = data[0].Length;
			_standardDeviations = new double[size];
			for (int i = 0; i < size; i++)
			{
				double counter = 0;
				double value = 0;

				for (int j = 0; j < data.GetLength(0); j++)
				{
					value += Math.Pow(data[j][i] - _means[i], 2);
					counter++;
				}
				_standardDeviations[i] = Math.Sqrt(value / counter);
			}
			return _standardDeviations;
		}

		public double[] GetMeans()
		{
			return _means;
		}

		public double[] GetStandardDeviations()
		{
			return _standardDeviations;
		}
	}
}