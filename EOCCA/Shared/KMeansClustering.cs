﻿using Accord.MachineLearning;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace EOCCA.Shared
{
    public class KMeansClustering
    {
        public static List<Cluster> KMeans(double[][] points, int clusterNumber, double extraRadius, bool standarization = false, bool pca = false)
        {
			Standarization baseStd = new Standarization(points);

			double[][] trainSet = points.Select(x => x.ToArray()).ToArray();

            KMeans kmeans = new KMeans(clusterNumber);

            // Compute the algorithm, retrieving an integer array
            //  containing the labels for each of the observations
            KMeansClusterCollection clusters = kmeans.Learn(trainSet);

            int[] labels = clusters.Decide(trainSet);

            List<Cluster> clustersList = new List<Cluster>();
            for (int centroidIndex = 0; centroidIndex < clusters.Centroids.GetLength(0); centroidIndex++)
            {
                List<double[]> clusterPoints = new List<double[]>();
                for (int label = 0; label < labels.Length; label++)
                {
                    if (labels[label] == centroidIndex)
                    {
                        clusterPoints.Add(trainSet[label]);
                    }
                }
                Debug.Assert(clusters.Centroids[centroidIndex].Length == clusterPoints[0].Length, $"clusters.Centroids[centroidIndex].Length={clusters.Centroids[centroidIndex].Length}\nclusterPoints[0].Length={clusterPoints[0].Length}");
                Cluster clst = new Cluster(clusters.Centroids[centroidIndex], clusterPoints.ToArray(), extraRadius, standarization, pca, baseStd);
                clustersList.Add(clst);
            }

            return clustersList;
        }
    }
}
