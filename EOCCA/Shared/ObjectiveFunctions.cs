﻿using Accord.Statistics.Analysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace EOCCA.Shared
{
    public class ObjectiveFunctions
    {
        public static double CountDefaultObjectiveFunction(double recall, int positive, int all)
        {
            if(positive == 0 || all == 0)
            {
                return 0.0;
            }
            return Math.Pow(recall, 2) / ((double)positive / (double)all);
        }

        public static double CountMatthewsCorrelationCoefficient(ConfusionMatrix confusionMatrix)
        {
            double numerator = confusionMatrix.TruePositives * confusionMatrix.TrueNegatives - confusionMatrix.FalsePositives * confusionMatrix.FalseNegatives;
            //double x1 = confusionMatrix.TruePositives + confusionMatrix.FalsePositives;
            //double x2 = confusionMatrix.TruePositives + confusionMatrix.FalseNegatives;
            //double x3 = confusionMatrix.TrueNegatives + confusionMatrix.FalsePositives;
            //double x4 = confusionMatrix.TrueNegatives + confusionMatrix.FalseNegatives;

            //double denominator = x1 * x2 * x3 * x4;

            double denominator = (double)(confusionMatrix.TruePositives + confusionMatrix.FalsePositives) * 
                                           (double)(confusionMatrix.TruePositives + confusionMatrix.FalseNegatives) * 
                                           (double)(confusionMatrix.TrueNegatives + confusionMatrix.FalsePositives) * 
                                           (double)(confusionMatrix.TrueNegatives + confusionMatrix.FalseNegatives);

            return numerator / Math.Sqrt(denominator);
        }
    }
}
