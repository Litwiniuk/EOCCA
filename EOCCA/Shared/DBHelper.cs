﻿using Accord.Statistics.Analysis;
using ExperimentDatabase;
using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EOCCA.Shared
{
    public class DBHelper
    {
        private static string _dbName = "statistics.sqlite";

        public static bool CheckIsExperimentExists(string argsString)
        {
            try
            {
                // TODO: REMOVE THAT:
                //return false;
                SqliteConnection con = new SqliteConnection("Data Source=" + _dbName);
                con.Open();

                SqliteCommand cmd = new SqliteCommand($"SELECT count(*) FROM experiments WHERE in_commandline='{argsString}'", con);

                int count = Convert.ToInt32(cmd.ExecuteScalar());

                return count != 0;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public static void SaveExperiment(int seed, bool std, bool pca, double objectiveFunction, ClusteringMethods clusteringMethod, int clusters, int outClusters, double extraRadius, ConfusionMatrix confusionMatrix, ConfusionMatrix trainConfusionMatrix, int trainingPointsSize, string totalTime, string fileName, string info, string constraintsString, string inClustersStr, int validAll, int validPositive, int testAll, int testPositive)
        {
            string argsString = ConstructArgsString(seed, std, pca, clusters, extraRadius, clusteringMethod, trainingPointsSize, fileName, info);

            if (CheckIsExperimentExists(argsString))
            {
                return;
            }

            using (var database = new Database(_dbName)) // utworzenie połączenia do bazy
            {
                using (var experiment = database.NewExperiment()) //utworzenie nowego eksperymentu(wiersz w tabeli experiments)
                {
                    try
                    {
                        experiment["in_commandline"] = argsString;
                        var constraints = experiment.NewChildDataSet("constraints"); // utworzenie powiązanej kolekcji "constraints" z eksperymentem; można w ten sposób tworzyć wiele powiązanych kolekcji. Nazwa odpowiada nazwie tabeli, w której zostanie ona zapisana. W bazie będzie powiązanie kluczem obcym "parent" w tabeli "constraints" do kolumny "id" w tabeli "experiments"
                        constraints["seed"] = seed;
                        constraints["std"] = std;
                        constraints["pca"] = pca;
                        constraints["in_clusters"] = clusters;
                        constraints["clustering_method"] = clusteringMethod == ClusteringMethods.XMEANS ? "XMeans" : (clusteringMethod == ClusteringMethods.PyXMEANS ? "PyXmeans" : "KMeans");
                        constraints["out_clusters"] = outClusters;
                        constraints["extraRadius"] = extraRadius;
                        constraints["file"] = fileName;
                        constraints["benchmark"] = string.IsNullOrEmpty(fileName) ? null : Regex.Match(fileName, @"^(\D)+_(\d)+_(\d)+").Groups?[0].Value;
                        constraints["accuracy"] = confusionMatrix?.Accuracy;
                        constraints["precision"] = confusionMatrix?.Precision;
                        constraints["recall"] = confusionMatrix?.Recall;
                        constraints["fscore"] = confusionMatrix?.FScore;
                        constraints["true_positives"] = confusionMatrix?.TruePositives;
                        constraints["true_negatives"] = confusionMatrix?.TrueNegatives;
                        constraints["false_positives"] = confusionMatrix?.FalsePositives;
                        constraints["false_negatives"] = confusionMatrix?.FalseNegatives;

                        constraints["train_accuracy"] = trainConfusionMatrix?.Accuracy;
                        constraints["train_precision"] = trainConfusionMatrix?.Precision;
                        constraints["train_recall"] = trainConfusionMatrix?.Recall;
                        constraints["train_fscore"] = trainConfusionMatrix?.FScore;
                        constraints["train_true_positives"] = trainConfusionMatrix?.TruePositives;
                        constraints["train_true_negatives"] = trainConfusionMatrix?.TrueNegatives;
                        constraints["train_false_positives"] = trainConfusionMatrix?.FalsePositives;
                        constraints["train_false_negatives"] = trainConfusionMatrix?.FalseNegatives;

                        constraints["objectiveFunction"] = objectiveFunction;
                        constraints["k"] = string.IsNullOrEmpty(fileName) ? null : Regex.Match(fileName, @"^(\D)+_(\d)+_(\d)+").Groups?[3].Value;
                        constraints["n"] = string.IsNullOrEmpty(fileName) ? null : Regex.Match(fileName, @"^(\D)+_(\d)+_(\d)+").Groups?[2].Value;
                        constraints["time"] = totalTime;
                        constraints["trainingPointsSize"] = trainingPointsSize;
                        constraints["valid_all"] = validAll;
                        constraints["valid_positive"] = validPositive;
                        constraints["test_all"] = testAll;
                        constraints["test_positive"] = testPositive;
                        constraints["info"] = info;
                        constraints["date"] = DateTime.Now.ToString();
                        constraints["in_clusters_str"] = inClustersStr;
                        constraints["matthewsCorrelationCoefficient"] = confusionMatrix == null? 0: ObjectiveFunctions.CountMatthewsCorrelationCoefficient(confusionMatrix);
                        constraints["constraints"] = constraintsString;

                        float validProbability = (float)validPositive / (float)validAll;
                        if (confusionMatrix != null)
                        {
                            constraints["fc1"] = Math.Pow(confusionMatrix.Recall, 2) - validProbability; // f_recall2-Pr
                            constraints["fc2"] = confusionMatrix.Recall - validProbability; // f_recall-Pr
                            constraints["fc3"] = confusionMatrix.Recall == 0 ? 0 : 2 / ((1 / confusionMatrix.Recall) + validProbability); // 2/(1/recall+Pr)
                            constraints["fc4"] = confusionMatrix.Recall == 0 || validProbability == 0 ? 0 : 2 / ((1 / confusionMatrix.Recall) - (1 / validProbability)); // 2/(1/recall-1/Pr)
                        }
                    }

                    catch (Exception e)
                    {
                        experiment["error"] = e.Message;// zapisanie informacji o błędzie
                        throw;
                    }

                    finally
                    {
                        experiment.Save(); // zapisanie wyników do bazy.Klauzula finally na wypadek wyjątku gwarantuje zapisanie zebranych danych
                    }
                }
            }
        }

        public static string ConstructArgsString(int seed, bool std, bool pca, int clusters, double extraRadius, ClusteringMethods clusteringMethod, int trainingPointsSize, string fileName = "", string info = "")
        {
            string clusteringMethodName = clusteringMethod == ClusteringMethods.XMEANS ? "xmeans" : (clusteringMethod == ClusteringMethods.PyXMEANS ? "pyxmeans" : "kmeans");
            return String.Format($"seed:{seed}.std:{std},pca:{pca},clusters:{clusters},extraRadius:{extraRadius},algorithm:{clusteringMethodName},training:{trainingPointsSize},file:{fileName},info:{info}");
        }
    }
}
