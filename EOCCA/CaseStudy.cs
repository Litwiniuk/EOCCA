﻿using EOCCA.Shared;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Statistics.Analysis;

namespace EOCCA
{
    public class CaseStudy
    {
        public void Start()
        {
            double[][] points = FileHelper.ReadDataSetFromCSV($"../../Data/Train/Rice-nonoise2.csv").Dataset;
            int maxSeed = 20;
            float extraRadius = 1.0f;
            int clusters = 1; // points[0].Length;
            for(int i = 0; i < maxSeed; i++)
            {
                Console.WriteLine("seed");
                SingleExperiment(i, points, extraRadius, clusters, true, true);
            }
        }

        public void SingleExperiment(int seed, double[][] points, float extraRadius, int clusters, bool std, bool pca)
        {
            Accord.Math.Random.Generator.Seed = seed;
            Process currentProcess = Process.GetCurrentProcess();
            currentProcess.Refresh();
            double startTime = currentProcess.TotalProcessorTime.TotalSeconds + PythonExe.TotalProcessorTime;
            var model = new EOCCA(Shared.ClusteringMethods.PyXMEANS, clusters, extraRadius, false, std, pca);
            model.Fit(points);
            currentProcess.Refresh();
            double totalTime = currentProcess.TotalProcessorTime.TotalSeconds + PythonExe.TotalProcessorTime - startTime;

            bool[] yPredictTrain = model.Predict(points);
            ConfusionMatrix trainConfusionMatrix = Program.MakeConfusionMatrix(Enumerable.Repeat(true, points.Length).ToArray(), yPredictTrain);

            int outClusters = model.GetClustersCount();
            //string constraints = string.Join("\n", this.replaceVariables(model.GetConstraints()));
            var constraints = this.replaceVariables(model.ToAMPL());

            DBHelper.SaveExperiment(seed, std, pca, 0, ClusteringMethods.PyXMEANS, clusters, outClusters, extraRadius, null, trainConfusionMatrix, points.Length, totalTime.ToString(), "case_study", "", constraints, "", 0, 0, 0, 0);
        }

        public string replaceVariables(string constaint)
        {
            return constaint.Replace("x0", "A").Replace("x1", "V").Replace("x2", "B").Replace("x3", "S").Replace("x4", "U").Replace("x5", "Ph").Replace("x6", "Pe").Replace("x7", "L");
        }
    }
}
