﻿using Accord.Statistics.Analysis;
using EOCCA.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace EOCCA
{
    public class PrepareData
    {
        public PrincipalComponentAnalysis _principalComponentAnalysis { get; private set; }
        public Standarization _std { get; private set; }
        private bool isStd = false;
        private bool isPca = false;

        public PrepareData(double[][] points, bool std = false, bool pca = false, Standarization baseStandardization = null)
        {
            isPca = pca;
            isStd = std;
            if (std)
            {
                _std = new Standarization(points, baseStandardization);
                points = _std.Transform(points); // to learn PCA on standardized data if enabled
            }
            if (pca)
            {
                _principalComponentAnalysis = new PrincipalComponentAnalysis(PrincipalComponentMethod.Center, false, points[0].Length);

                // make sure that we supply PCA with at least the same number of rows as columns
                var numDuplicates = (int)Math.Ceiling((double)points[0].Length / points.Length);
                if (numDuplicates > 1)
                {
                    var newPoints = new double[numDuplicates * points.Length][];
                    for (int d = 0; d < numDuplicates; ++d)
                    {
                        for (int p = 0; p < points.Length; ++p)
                        {
                            newPoints[d * points.Length + p] = points[p]; // shallow copy
                        }
                    }
                    points = newPoints;
                }

                _principalComponentAnalysis.Learn(points);
                Debug.Assert(_principalComponentAnalysis.NumberOfInputs == _principalComponentAnalysis.NumberOfOutputs);
            }
        }


        public double[][] Transform(double[][] points)
        {
            double[][] transformed = points;
            if (isStd)
            {
                transformed = _std.Transform(points);
                Debug.Assert(!object.ReferenceEquals(transformed, points));
                Debug.Assert(transformed.Length == points.Length);
                Debug.Assert(transformed[0].Length == points[0].Length);
            }
            if (isPca)
            {
                transformed = _principalComponentAnalysis.Transform(transformed);
                Debug.Assert(!object.ReferenceEquals(transformed, points));
                Debug.Assert(transformed.Length == points.Length);
                Debug.Assert(transformed[0].Length == points[0].Length);
            }
            return transformed;
        }
    }
}
