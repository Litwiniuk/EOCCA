option gurobi_options 'intfeastol=1e-9 feastol=1e-9 opttol=1e-9 barconvtol=1e-12 barqcptol=1e-12 outlev=1 numericfocus=3 presolve=0 mipgap=1e-6 feasrelaxbigm=1e12 quad=1';
solve;
display A,V,B,S,U,Ph,Pe,L;
display b;

