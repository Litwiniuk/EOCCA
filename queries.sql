select benchmark, file, fscore, constraints,
"(" || replace(replace(replace(replace(replace(constraints, "x0", "x[1]"), "x1", "x[2]"), "x2", "x[3]"), char(10),") || " || char(10) || "("), "E", "*^") || ")" AS mathematica
from constraints 
where info LIKE 'tuning_trainset_%'
AND benchmark LIKE '%_3_%'
GROUP BY benchmark
HAVING fscore=MAX(fscore);

CREATE VIEW constraintsTransformed AS
SELECT
IFNULL(fscore, 0.0) AS fscore,
*
FROM constraints;

SELECT train_recall, out_clusters, constraints
FROM constraints
WHERE benchmark == ""
ORDER BY train_recall DESC;

