﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EOCCA.Shared;
using Accord.Statistics.Analysis;

namespace EOCCA_Tests
{
    [TestClass]
    public class ObjectiveFunctionsTest
    {
        [TestInitialize()]
        public void Initialize()
        {

        }

        [TestMethod]
        public void MatthewsCorrelationCoefficient()
        {
            int tp = 5508;
            int tn = 92461;
            int fp = 1201;
            int fn = 830;

            double trueResult = 0.833863601;


            ConfusionMatrix confusionMatrix = new ConfusionMatrix(tp, fn, fp, tn);
            var result = ObjectiveFunctions.CountMatthewsCorrelationCoefficient(confusionMatrix);

            Assert.AreEqual(Math.Round(trueResult, 5), Math.Round(result, 5));
        }
    }
}
