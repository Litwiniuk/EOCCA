﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EOCCA.Shared;

namespace EOCCA_Tests
{
    [TestClass]
    public class StandarizationTest
    {
        private double[][] testSet;
        private double[] realMeans;

        [TestInitialize()]
        public void Initialize()
        {
            testSet = new double[3][];

            testSet[0] = new double[] { 4, 12 };
            testSet[1] = new double[] { 1.5, 2.5 };
            testSet[2] = new double[] { 0.5, 3.5 };

            realMeans = new double[] { 2.0, 6.0 };
        }

        [TestMethod]
        public void TestMeans()
        {
            Standarization std = new Standarization(testSet);

            double[] testMeans = std.Mean(testSet);

            bool isEqual = Enumerable.SequenceEqual(testMeans, realMeans);
            Assert.IsTrue(isEqual);
        }

        [TestMethod]
        public void TestStandardDeviations()
        {
            double standardDeviation1d = Math.Sqrt((4 + 0.25 + 2.25) / 3);
            double standardDeviation2d = Math.Sqrt((36 + 12.25 + 6.25) / 3);

            Standarization std = new Standarization(testSet);
            double[] testDeviations = std.StandardDeviations(testSet);


            double[] realDeviations = new double[2] { standardDeviation1d, standardDeviation2d };
            bool isEqual = Enumerable.SequenceEqual(testDeviations, realDeviations);
            Assert.IsTrue(isEqual);
        }

        [TestMethod]
        public void TestTransform()
        {
            Standarization std = new Standarization(testSet);

            double standardDeviation1d = Math.Sqrt((4 + 0.25 + 2.25) / 3);
            double standardDeviation2d = Math.Sqrt((36 + 12.25 + 6.25) / 3);

            double[][] realSet = new double[3][];

            realSet[0] = new double[] { (4 - 2) / standardDeviation1d, (12 - 6) / standardDeviation2d };
            realSet[1] = new double[] { (1.5 - 2) / standardDeviation1d, (2.5 - 6) / standardDeviation2d };
            realSet[2] = new double[] { (0.5 - 2) / standardDeviation1d, (3.5 - 6) / standardDeviation2d };

            double[][] transformed = std.Transform(testSet);


            ////Round 3 decimal places
            //realSet = realSet.Select(x => x.Select(y => Convert.ToDouble(y.ToString("N3"))).ToArray()).ToArray();
            //transformed = transformed.Select(x => x.Select(y => Convert.ToDouble(y.ToString("N3"))).ToArray()).ToArray();

            bool isEqual = true;

            for(int i = 0; i < realSet.Length; i++)
            {
                for (int j = 0; j < realSet[i].Length; j++)
                {
                    if(realSet[i][j] != transformed[i][j])
                    {
                        isEqual = false;
                        break;
                    }
                }
                if(!isEqual)
                {
                    break;
                }
            }
            Assert.IsTrue(isEqual);
        }
    }
}
